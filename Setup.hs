import Distribution.PackageDescription ( PackageDescription(..) )
import Distribution.Simple
import Distribution.Simple.InstallDirs ( mandir, CopyDest(NoCopyDest) )
import Distribution.Simple.LocalBuildInfo ( LocalBuildInfo(..), absoluteInstallDirs )
import Distribution.Simple.Setup ( copyDest, copyVerbosity, fromFlag, installVerbosity )
import Distribution.Simple.Utils ( installOrdinaryFiles )
import Distribution.Verbosity ( Verbosity )
import System.FilePath ( (</>) )

main :: IO ()
main = defaultMainWithHooks $ simpleUserHooks
    { postCopy = \ _ flags -> installManpages (fromFlag $ copyVerbosity flags) (fromFlag $ copyDest flags)
    , postInst = \ _ flags -> installManpages (fromFlag $ installVerbosity flags) NoCopyDest }

installManpages :: Verbosity -> CopyDest -> PackageDescription -> LocalBuildInfo ->  IO ()
installManpages verbosity copy pkg lbi =
  installOrdinaryFiles verbosity (mandir (absoluteInstallDirs pkg lbi copy) </> "man1") []

