# R-Candy

This was a complete reboot of our attempts at a mapper/aligner.  We take
a few lessons learned from Anfo, apply modern algorithms, and do it in a
civilized language.  Unfortunately, it was abandoned half way in, and is
no longer maintained.  

The central idea is to use a succinct full text index (from the FM-index
family), which allows matching without any annoying seed heuristic.  The
resulting flexibility will be put to good use when mapping bisulphite
treated reads, ancient DNA, 4-thio-uracil and whatever else modifies the
DNA in some way.

This is nowhere near complete.  The algorithm is naive and will not
work well for long reads.  It does implement a model for damage expected
on ancient DNA.  We intendeded to use it on really bad DNA:  fragments
below about 40 residues with deamination, but in its current state, it
is too slow even in that application.

Installation
------------

`r-candy` uses Cabal, the standard installation mechanism for
Haskell.  It depends on the `biohazard` library and additional stuff
from Hackage.  To install, follow these steps:

* install GHC (see http://haskell.org/ghc)
  and cabal-install (see http://haskell.org/cabal),
* `cabal update` (takes a while to download the current package list),
* `git clone https://ustenzel@bitbucket.org/ustenzel/r-candy.git`
* `cabal install r-candy/`

When done, on an unmodified Cabal setup, you will find the binaries in
`${HOME}/cabal/bin`.  Cabal can install them in a different place, please
refer to the Cabal documentation at http://www.haskell.org/cabal/ if
you need that.  Sometimes, repeated installations and re-installations can result
in a thoroughly unusable state of the Cabal package collection.  If you get error
messages that just don't make sense anymore, please refer to
http://www.vex.net/~trebla/haskell/sicp.xhtml; among other useful things, it
tells you how to wipe a package database without causing more destruction.
