# Algorithm

## Primary Plan

We will use the simple version of the alphabet-friendly FM-index.
Components should be somewhat exchangeable, but here's the primary plan:
We index a genome on both strands.  The index will be the BWT of the
genome, stored as Huffman-shaped wavelet tree (gives implicit
compression to H0, a very simple index, scales nicely to large
alphabets, and allows change of direction).  The lookup cost that's
logarithmic in the alphabet size shouldn't be so bad: we'll often look
at all branches anyway, and then the cost stays linear in the alphabet
size.  We will add special marker characters for positions where we
sample the suffix array.  The Huffman tree used is (allowed to be) known
at build time to allow optimizations in the aligner.

The algorithm should run like this:

* Read the genome, split into contigs, count total length, and dump the
  transcoded data including markers into a flat array.
  (`parseGenomeFile`, `parseGenomeHandle`)

* Append the second strand and add terminator characters.
  (`appendSecondStrand`)  Alternatively, just append terminators.

* Create suffix array using multi-key-quicksort.  This works fine, but
  an algorithm (e.g. "DC3") that doesn't have worst case quadratic
  runtime would be nice.  (`suffixArray`)

* Find the marked portion, store the suffix array sample and some meta
  data.  (`createIndex`)

* Read off wavelet tree from suffix array and store as
  Rank9 structures.  (`createIndex`, `mkWavelet`)


An index on disc therefore consists of:

* the Huffman tree with histogram,
* the list of contigs with coordinates,
* the SA sample(s),
* the wavelet tree.

After restoring it into memory, we derive the "C" array from the Huffman
tree, and implement the "Occ" function based on the wavelets (`restoreIndex`).


## Alternate Designs

### Marker Characters

It would be good to avoid the marker character.  One way to avoid it is
to use a suitable character (none is available), another is to sample
the BWT at regular intervals (turns out to be too unevenly distributed
in the untransformed genome).  The third option is to sample the BWT
non-uniformly and mark the sampled positions other than by a marker
character.  This is possible, but needs a separate rank-select structure
for a very sparse bit array.

### Index Construction

Technically, indexing one strand of the genome would be enough.
However, by indexing both strands, we only need to align the query
sequence itself (not its reverse-complement), and we gain the ability to
move in both directions over the genome.  Changing direction is
essentially backtracing an alignment and retracing the same steps in the
reverse order while complementing the matched characters.


### Bidirectional Wavelet Tree

The bidirectional wavelet tree allows the synchronization of forward and
backward search while arbitrarily interleaving those two.  We don't need
the interleaving (at least not yet), and reversing direction by
backtracing once is cheaper than keeping track of a reverse match.  So
the bidirectional wavelet tree is uninteresting right now, but remains a
viable option, should the need arise.



# Notes

* Suffix array construction uses 0 as terminator/sentinel, binary
  encoding of wavelet trees uses 0 as singular value.  We need to avoid
  using 0 as actual character, and the tree construction algorithms in
  Bio.Candy.Huffman take care of that.

* Nothing in here will work well for realistic genomes unless Int is at
  least about 35 bits wide.  In other words, you need a 64bit machine
  and a 64bit Haskell implementation.  Since the Rank9 data structure
  pretty much requires 64 bit operation, that restriction is unlikely to
  be lifted.

* The actual genome (`hg19_evan.2`) is composed as follows:

    |base|frequency|
    |---|---|
    |A|856481589|
    |B|3|
    |C|592911644|
    |G|593462230|
    |H|2|
    |K|156|
    |M|192|
    |N|237114368|
    |R|544|
    |S|135|
    |T|857494067|
    |V|2|
    |W|126|
    |Y|519|

  or if we simply collapse the ambiguity codes:

    |base|frequency|
    |---|---|
    |A|856481589|
    |C|592911644|
    |G|593462230|
    |N|237116047|
    |T|857494067|

  so 2.9Gb of payload and some crap.
