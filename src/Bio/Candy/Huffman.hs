module Bio.Candy.Huffman where

import Bio.Prelude
import Data.Array.ST            ( runSTUArray, newArray, readArray, writeArray )
import Data.Array.Unboxed       ( UArray, array, (!), assocs )

import qualified Data.ByteString.Lazy       as L
import qualified Data.ByteString            as S
import qualified Data.Map                   as M

type Hist = UArray Word8 Int

-- Separator character.  This is ASCII US, which both follows some logic
-- and is unlikely to collide with actual input.
sep :: Word8
sep  = 30

mkHist :: [ L.ByteString ] -> Hist
mkHist ss = runSTUArray
    (do a <- newArray (minBound,maxBound) 0
        flip mapM_ ss (\s -> addA a sep 1 >> addS a s)
        return a)
  where
    addA a i x = readArray a i >>= writeArray a i . (+) x
    addC a i k = addA a i 1 >> k

    -- argl, can't use L.foldr, since it has an accumulator, which causes stack overflow.
    -- can't use L.foldl', since I'm in ST, and there is no L.foldlM.
    -- could use a loop, but this Haskell, not C, dammit!
    -- at least the following should fuse and not be too inefficient then.
    addS a s = mapM_ (foldr (addC a) (return ()) . S.unpack) $ L.toChunks s

-- We always relabel everything when constructing the Huffman tree, thus
-- making sure our Huffman trees are also binary search trees.  The key
-- is always a Word8 (if you need a bigger alphabet, you have other
-- problems...).
data HuffTree a b l = Leaf !Word8 l | Branch !Word8 a b (HuffTree a b l) (HuffTree a b l)
    deriving ( Show, Read )

instance Functor (HuffTree a b) where
    fmap f (Leaf w a) = Leaf w (f a)
    fmap f (Branch w a b l r) = Branch w a b (fmap f l) (fmap f r)

{-# INLINE maxSymbol #-}
maxSymbol :: HuffTree a b l -> Word8
maxSymbol (Branch _ _ _ _ r) = maxSymbol r
maxSymbol (Leaf s _) = s

{-# INLINE minSymbol #-}
minSymbol :: HuffTree a b l -> Word8
minSymbol (Branch _ _ _ l _) = minSymbol l
minSymbol (Leaf s _) = s

-- Creates a Huffman tree from a histogram.  The finishes tree is then
-- labelled, so it becomes a binary search tree.  The necessary
-- translation tables can then be read off.  Relabelling does not use
-- the code 0 (reserved for handling of edge cases in suffix array
-- construction).
mkHufftree :: Hist -> HuffTree () () Word8
mkHufftree h = relabel $ unroll $ M.fromList [ (f, leaf w) | (w,f) <- assocs h, f /= 0 ]
  where
    unroll m = case M.minViewWithKey m of {
        Just ((f1,t1),m')  -> case M.minViewWithKey m' of {
          Just ((f2,t2),m'') -> unroll $ M.insert (f1+f2) (branch t1 t2) m'' ;
          Nothing -> t1 } ;
        Nothing -> error "mk_hufftree: empty list" }

relabel :: HuffTree a b c -> HuffTree a b c
relabel t = go 1 t (const id)
  where
    go acc0 (Leaf _ a) k = (k $! acc0+1) $! Leaf acc0 a
    go acc0 (Branch _ a b l r) k =
        go acc0 l              $ \acc1 l' ->
        go acc1 r              $ \acc2 r' ->
        k acc2 $! Branch acc1 a b l' r'

injectTable, projectTable :: HuffTree a b Word8 -> (Word8 -> Word8)
projectTable t = (a !) where a = array (minBound, maxBound) $ huffAssocs t :: UArray Word8 Word8
injectTable  t = (a !) where a = array (minBound, maxBound) [ (y,x) | (x,y) <- huffAssocs t ] :: UArray Word8 Word8

{-# INLINE huffAssocs #-}
huffAssocs :: HuffTree a b c -> [(Word8, c)]
huffAssocs t = go t []
  where
    go (Leaf w a) = (:) (w,a)
    go (Branch _ _ _ l r) = go l . go r

{-# INLINE branch #-}
branch :: HuffTree () () a -> HuffTree () () a -> HuffTree () () a
branch = Branch 0 () ()

{-# INLINE leaf #-}
leaf :: a -> HuffTree () () a
leaf = Leaf 0

-- | Huffman tree, calculated from the histogram over the unmasked
-- chromosome hg18:chr21.  Will be used for ordinary genomes (no
-- ambiguity codes or similar fanciness).
unmaskedGenomeTree :: HuffTree () () Word8
unmaskedGenomeTree = relabel $ fmap c2w $ branch
      (branch
        (branch
          (branch (leaf 'N') (leaf (w2c sep)))
          (leaf 'C'))
        (leaf 'G'))
      (branch (leaf 'T') (leaf 'A'))

-- | Huffman tree for masked genomes.  Similar to @unmaskedGenomeTree@.
maskedGenomeTree :: HuffTree () () Word8
maskedGenomeTree = relabel $ fmap c2w $ branch
      (branch
        (branch
          (branch
            (branch
              (branch (leaf 'n') (leaf 'N'))
              (leaf (w2c sep)))
            (leaf 'c'))
          (leaf 'C'))
        (branch (leaf 'g') (leaf 'G')))
      (branch
        (branch (leaf 't') (leaf 'a'))
        (branch (leaf 'T') (leaf 'A')))

