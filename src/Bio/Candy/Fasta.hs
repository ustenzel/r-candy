module Bio.Candy.Fasta (
        Genome, Scaffold(..), Contig(..),
        makeComplFunc, defaultComplFunc,
        putContigTable, getContigTable,
        appendSecondStrand,
        ScanEnv(..), parseGenomeHandle, parseGenomeFile
                      ) where

import qualified Data.ByteString as S
import qualified Data.IntMap     as I

import Bio.Prelude               hiding ( Error )
import Control.Monad.Trans.Reader       ( ReaderT, runReaderT, ask, asks )
import Data.Array.Unboxed               ( UArray, listArray, (!), (//) )
import Data.Binary.Get                  ( Get )
import Data.Binary.Put                  ( Put )

import Bio.Candy.Array
import Bio.Candy.Huffman
import Bio.Candy.Util

-- $fastaparser
-- Parser for typical UCSC-style genomes.  We parse FastA, creating a
-- gap symbol after each sequence.  We treat lone LF or CR as line
-- break, space and tab as white space; long stretches of 'N' (the
-- meaning of "long" is configurable) turn into gaps, '>' signifies
-- headers.  Everything else is not interpreted and is passed through an
-- injection function dependent on the alphabet, which must be known
-- beforehand.  The genome is broken into a list of scaffolds, each of
-- which is broken into contigs.


-- | A genome, but only meta information, in fact.  We transcode the
-- actual sequence directly into an array, but the meta information is
-- represented here.
type Genome = [ Scaffold ]

data Scaffold = Scaffold {
    sc_name :: !S.ByteString,           -- ^ name from FastA header
    sc_description :: !S.ByteString,    -- ^ everything else from header (incl. blanks)
    sc_contigs :: !(I.IntMap Contig),   -- ^ offset-to-contig mapping (all sequence except stretches of Ns)
    sc_inv_contigs :: I.IntMap Contig,  -- ^ sa-to-contig mapping (lazy for a reason)
    sc_length :: !Int,                  -- ^ total length (incl. leading and trailing Ns)
    sc_num :: !Int }                    -- ^ running number
  deriving Show

-- | Meta data for a contig.  We mostly need this to translate between
-- suffix array and genome coordinates.  Note that the offset into the
-- array includes markers already placed in it, but the length doesn't.
-- Coordinate translation will need to compensate for those inside the
-- contig.
data Contig = Contig {
    co_offset_in_scaffold :: !Int,      -- ^ offset in the input genome
    co_offset_in_array :: !Int,         -- ^ offset of the complemented copy in array
    co_length :: !Int,                  -- ^ length in bases
    co_num :: !Int }                    -- ^ running number
  deriving Show

-- | Codes a genome in compact binary representation.
putContigTable :: Genome -> Put
putContigTable = putList putScaffold
  where
    putScaffold s = do putString $ sc_name s
                       putString $ sc_description s
                       putList putContig . I.elems $ sc_contigs s
                       putWord $ sc_length s

    putContig c = do putWord $ co_offset_in_scaffold c
                     putWord $ co_offset_in_array c
                     putWord $ co_length c

getContigTable :: Get Genome
getContigTable = renumber 0 0 <$> getList getScaffold
  where
    renumber :: Int -> Int -> [Int -> Int -> Scaffold] -> [Scaffold]
    renumber  _  _ [    ] = []
    renumber !c !s (f:fs) = sc : scs
        where !sc  = f c s
              !scs = renumber (c + I.size (sc_contigs sc)) (s+1) fs

    getScaffold :: Get (Int -> Int -> Scaffold)
    getScaffold = mkScaffold <$> getString <*> getString <*> getContigList <*> getWord

    getContigList :: Get ([Int -> Contig])
    getContigList = getList (Contig <$> getWord <*> getWord <*> getWord)


mkScaffold :: Enum t => S.ByteString -> S.ByteString -> [t -> Contig] -> Int -> t -> Int -> Scaffold
mkScaffold u v cs w ini ini2 = sc
  where sc    = Scaffold u v c_fwd c_rev w ini2
        c_fwd = I.fromList [ c `seq` (co_offset_in_scaffold c, c) | c <- zipWith ($) cs [ini..] ]
        c_rev = I.fromList [         (co_offset_in_array    c, c) | c <- I.elems (sc_contigs sc) ]

makeComplFunc :: [(Char,Char)] -> (Word8 -> Word8)
makeComplFunc pairs = (myarr !)
  where myarr = listArray (minBound, maxBound) (range (minBound, maxBound)) //
                ( [ (c2w a, c2w b) | (a,b) <- pairs ] ++ [ (c2w a, c2w b) | (b,a) <- pairs ] )
                :: UArray Word8 Word8

defaultComplFunc :: Word8 -> Word8
defaultComplFunc = makeComplFunc $ zip (both "ACBDMR") (both "TGVHKY")
  where both s = map toUpper s ++ map toLower s


{-# INLINE appendSecondStrand #-}
appendSecondStrand :: IORef Vec -> (Word8 -> Word8) -> IO ()
appendSecondStrand v c = do
    Vec len_ _ fp <- readIORef v
    let len = fromIntegral len_
    let total = 2*len + 4

    fp' <- mallocForeignPtrBytes total
    withForeignPtr fp' $ \dest ->
      withForeignPtr fp  $ \source -> do
        peekElemOff source 0 >>= pokeElemOff dest len -- copy initial to terminal nul
        forM_ [1..len] $ \i -> do                     -- copy rest
            w <- peekElemOff source (len-i)
            pokeElemOff dest (len-i) w                -- forwards
            pokeElemOff dest (len+i) (c w)            -- and complemented

        pokeElemOff dest (2*len+1) 0    -- three terminating zeroes
        pokeElemOff dest (2*len+2) 0
        pokeElemOff dest (2*len+3) 0

    writeIORef v $! Vec total total fp'

data ScanEnv = ScanEnv { se_genome :: IORef Vec
                       , se_num_n  :: Int
                       , se_inject :: Word8 -> Word8
                       , se_report :: String -> IO () }

data Stream c = Chunk !c | Void | EOF

data Step i m o = Continue !(Stream i -> Iteratee i m o)
                | Yield !o !(Stream i)
                | Error !String

runStep :: Step S.ByteString m o -> IO o
runStep (Error        err ) = error err
runStep (Continue       _ ) = error "unexpected end-of-file"
runStep (Yield o      EOF ) = return o
runStep (Yield _     Void ) = error "incomplete parse at no current input"
runStep (Yield _ (Chunk c)) = error $ "incomplete parse at " ++ show (S.take 16 c)

newtype Iteratee i m o = Iteratee { runIteratee :: m (Step i m o) }

type Enumerator a m b = Step a m b -> Iteratee a m b

instance Monad m => Functor (Step i m) where
    fmap f (Continue k) = Continue (fmap f . k)
    fmap f (Yield o s) = Yield (f o) s
    fmap _ (Error err) = Error err

instance Monad m => Functor (Iteratee i m) where
    fmap f it = Iteratee $ liftM (fmap f) $ runIteratee it

instance Monad m => Applicative (Iteratee i m) where
    pure = return
    f <*> a = f >>= flip fmap a

instance Monad m => Monad (Iteratee i m) where
    return a = yieldI a Void
    m >>= f  = Iteratee $ runIteratee m >>= \r1 -> case r1 of
                    Continue k -> return $! Continue ((>>= f) . k)
                    Error err  -> return $! Error err
                    Yield a s1 -> runIteratee (f a) >>= \r2 -> case r2 of
                        Continue k -> runIteratee (k s1)
                        Error err  -> return $! Error err
                        Yield y _  -> return $! Yield y s1

liftI :: Monad m => m a -> Iteratee i m a
liftI m = Iteratee $ m >>= \r -> return $! Yield r Void

infixr 9 $$
($$) :: Monad m => Enumerator a m b -> Iteratee a m b -> Iteratee a m b
enum $$ iter = Iteratee $ runIteratee iter >>= runIteratee . enum

errorI :: Monad m => String -> Iteratee i m o
errorI err = Iteratee $ return $! Error err

yieldI :: Monad m => o -> Stream i -> Iteratee i m o
yieldI b rest = Iteratee $ return $! Yield b rest

continueI :: Monad m => (Stream i -> Iteratee i m o) -> Iteratee i m o
continueI k = Iteratee $ return $! Continue k

parseGenomeHandle :: ScanEnv -> Handle -> IO Genome
parseGenomeHandle se h = runStep =<< (flip runReaderT se $ runIteratee $ enumerateHandle h $$ genomeI)

parseGenomeFile :: ScanEnv -> FilePath -> IO Genome
parseGenomeFile se fp  = runStep =<< (flip runReaderT se $ runIteratee $ enumerateFile fp $$ genomeI)

enumerateFile :: FilePath -> Enumerator S.ByteString (ReaderT env IO) b
enumerateFile fp iter = Iteratee $ do
    env <- ask
    liftIO $ withBinaryFile fp ReadMode $ \h ->
        runReaderT (runIteratee $ enumerateHandle h iter) env

enumerateHandle :: Handle -> Enumerator S.ByteString (ReaderT env IO) b
enumerateHandle h = go
  where
    bufsize = 32*1024 - 100

    go (Error exc) = errorI exc
    go (Yield b rest) = yieldI b rest
    go (Continue k) = Iteratee $ do chunk <- liftIO $ S.hGet h bufsize
                                    if S.null chunk
                                        then runIteratee (k EOF)
                                        else runIteratee (k (Chunk chunk)) >>= runIteratee . go


genomeI :: Iteratee S.ByteString (ReaderT ScanEnv IO) Genome
genomeI = fixup 0 0 `liftM` (continueI (junk True) >> repeatI seqI)
  where
    -- Iterate over junk, optionally starting at beginnning of a line.
    -- We scan for a '>' or '@', and if it's either at the beginning or
    -- preceeded by a line feed, the junk ends there.  If no such marker
    -- exists, we just check if we end with a line feed, and continue
    -- appropriately.
    junk _ EOF = yieldI () EOF
    junk at_sol Void = continueI $ junk at_sol
    junk at_sol (Chunk ch) | S.null ch = continueI $ junk at_sol
    junk at_sol (Chunk ch) =
        case S.findIndex (\c -> c == c2w '>') ch of
            Just 0 | at_sol    -> yieldI () (Chunk ch)
                   | otherwise -> junk False . Chunk $ S.tail ch
            Just i | S.index ch (i-1) == 13 -> yieldI () . Chunk $ S.drop i ch
                   | otherwise              -> junk False . Chunk $ S.drop i ch
            Nothing -> continueI $ junk $ S.last ch == 13

    fixup !si !ci (sc:scs) = case sc si ci of (sc', ci') -> sc' : fixup (si+1) ci' scs
    fixup _ _ [] = []

repeatI :: Monad m => Iteratee i m o -> Iteratee i m [o]
repeatI iter = isEofI >>= \e -> if e then return [] else liftM2 (:) iter (repeatI iter)

seqI :: Iteratee S.ByteString (ReaderT ScanEnv IO) (Int -> Int -> (Scaffold, Int))
seqI = do _ <- headI
          n <- nameI
          d <- descI
          (cs,l) <- bodyI (scaffoldI contigI)
          return $ \si ci -> (mkScaffold n d cs l si ci, ci + length cs)

headI :: Monad m => Iteratee S.ByteString m Word8
headI = continueI go
  where
    go EOF = errorI "headI at EOF"
    go Void = continueI go
    go (Chunk ch) = case S.uncons ch of
            Nothing    -> continueI go
            Just (h,t) -> yieldI h (Chunk t)

peekI :: Monad m => Iteratee i m (Maybe i)
peekI = continueI go
  where
    go EOF = yieldI Nothing EOF
    go Void = continueI go
    go (Chunk ch) = yieldI (Just ch) Void

isEofI :: Monad m => Iteratee i m Bool
isEofI = continueI go
  where
    go EOF = yieldI True EOF
    go Void = continueI go
    go (Chunk ch) = yieldI False (Chunk ch)


breakI :: Monad m => (Word8 -> Bool) -> Iteratee S.ByteString m S.ByteString
breakI f = continueI go
  where
    go EOF = yieldI S.empty EOF
    go Void = continueI go
    go (Chunk ch) =
        case S.findIndex f ch of
            Nothing -> continueI (fmap (S.append ch) . go)
            Just i  -> yieldI (S.take i ch) (Chunk $ S.drop i ch)


reportI :: String -> Iteratee i (ReaderT ScanEnv IO) ()
reportI msg = liftI $ do f <- asks se_report ; liftIO $ f msg

-- Iterate over a sequence identifier.  By definition, the id ends at
-- the first whitespace character, which is neither included nor
-- skipped.
nameI :: Iteratee S.ByteString (ReaderT ScanEnv IO) S.ByteString
nameI = do n <- breakI (\c -> c == 10 || c == 13 || c == 32 || c == 9)
           reportI $ "beginning scaffold " ++ show n
           return n

-- Iterate over a sequence description.  This is everything up to the
-- end of line, which is removed.
descI :: Monad m => Iteratee S.ByteString m S.ByteString
descI = do d <- breakI (\w -> w == 10 || w == 13)
           _ <- breakI (\w -> w /= 10 && w /= 13)
           return $ S.dropWhile (\c -> c == 9 || c == 32) d

-- Iterate body of sequence.  We scan the body's lines, remove useless
-- characters (blanks and line feeds) and pass the result on.
bodyI :: Monad m => Iteratee S.ByteString m a -> Iteratee S.ByteString m a
bodyI = outer True
  where
    outer at_sol inner = Iteratee $ runIteratee inner >>= \r1 -> return $! case r1 of
        Error err  -> Error err
        Yield _ _  -> Error "inner iteratee diverged"
        Continue k -> Continue (go at_sol k)

    go !_ k EOF = Iteratee $ runIteratee (k EOF) >>= \r -> return $! case r of
        Error err   -> Error err
        Yield a EOF -> Yield a EOF
        _           -> Error "inner iteratee diverged"

    go !at_sol k Void = continueI $ go at_sol k
    go !at_sol k (Chunk ch) | S.null ch = continueI $ go at_sol k

    -- check for next header only at start of chunk and after newline
    -- (incurs a few unnecessary recursions, but is easy)
    go !at_sol k (Chunk ch) | at_sol && S.head ch == c2w '>' =
        Iteratee $ runIteratee (k EOF) >>= \r -> return $! case r of
            Error err   -> Error err
            Yield a EOF -> Yield a (Chunk ch)
            _           -> Error "inner iteratee diverged"

    -- Find first blank character, we don't pass that one on.
    go _ k (Chunk ch) =
        case S.findIndex (\w -> w == 10 || w == 13 || w == 9 || w == 32) ch of
            Nothing -> outer False (k (Chunk ch))
            Just i -> Iteratee $ runIteratee (k (Chunk (S.take i ch))) >>= \r -> case r of
                Error err -> return $! Error err
                Yield _ _ -> return $! Error "inner iteratee diverged"
                Continue k2 -> case S.drop (i+1) ch of
                    rest | S.null rest -> return $! Continue (go new_sol k2)
                         | otherwise   -> runIteratee $ go new_sol k2 (Chunk rest)
                      where new_sol = S.index ch i == 10 || S.index ch i == 13

-- Breaks a string of bases at stretches of Ns.
scaffoldI :: Iteratee (Either Int S.ByteString) (ReaderT ScanEnv IO) a
         -> Iteratee S.ByteString (ReaderT ScanEnv IO) a
scaffoldI inner = liftI (asks se_num_n) >>= \n -> outer n inner
  where
    outer !maxn iter = Iteratee $ runIteratee iter >>= \r1 -> return $! case r1 of
        Error err  -> Error err
        Yield _ _  -> Error "inner iteratee diverged"
        Continue k -> Continue (go (Right S.empty) k)
      where
        go !sofar k EOF = Iteratee $ runIteratee (k (Chunk sofar)) >>= \r -> case r of
            Error err   -> return $! Error err
            Yield _ _   -> return $! Error "inner iteratee diverged"
            Continue k2 -> runIteratee (k2 EOF) >>= \r2 -> return $! case r2 of
                Error err   -> Error err
                Yield b EOF -> Yield b EOF
                _           -> Error "inner iteratee diverged"

        go !sofar k Void = continueI $ go sofar k
        go !sofar k (Chunk ch) | S.null ch = continueI $ go sofar k
        go !sofar k (Chunk ch) =
            case S.span (\w -> w == c2w 'N' || w == c2w 'n') ch of
                (_,cr) | S.null cr -> -- all Ns, we keep them and wait for the next chunk
                                      continueI $ go (sofar `appE` ch) k

                (cl,cr) | sofar /= Right S.empty || not (S.null cl) -> -- some Ns, pass them on
                    Iteratee $ runIteratee (k (Chunk (sofar `appE` cl))) >>= \r -> case r of
                        Error err -> return $! Error err
                        Yield _ _ -> return $! Error "inner iteratee diverged"
                        Continue k2 -> runIteratee $ go (Right S.empty) k2 (Chunk cr)

                _ -> case findNStretch ch of -- no Ns, we must split.
                        -- Pass on the left part, recurse on the right
                        -- part.  In the next round, the Ns will be at
                        -- the start and are handled above.
                        (cl,cr) -> Iteratee $ runIteratee (k (Chunk (Right cl))) >>= \r -> case r of
                            Error err -> return $! Error err
                            Yield _ _ -> return $! Error "inner iteratee diverged"
                            Continue k2 -> runIteratee $ go (Right S.empty) k2 (Chunk cr)

        appE (Left   x)  s = Left $! x + S.length s
        appE (Right s1) s2 = let l = S.length s1 + S.length s2
                             in if l > maxn then Left l else Right $! S.append s1 s2

        findNStretch s = let i = findNStretch' 0 s in (S.take i s, S.drop i s)
        findNStretch' i s = case S.findIndex (\w -> w == c2w 'N' || w == c2w 'n') (S.drop i s) of
            Nothing -> S.length s
            Just j  -> case S.findIndex (\w -> w /= c2w 'N' && w /= c2w 'n') (S.drop (i+j) s) of
                Nothing -> i+j
                Just k | k > maxn -> i+j
                       | otherwise -> findNStretch' (i+j+k) s

-- Regarding separators: we *start* each contig with a separator, but
-- don't terminate with one.  The final terminator must be appended
-- later, along with the guard characters for suffix array construction
-- (appendSecondStrand does that).
contigI :: Iteratee (Either Int S.ByteString) (ReaderT ScanEnv IO) ([Int -> Contig], Int)
contigI = goN 0
  where
    goN !total = peekI >>= \s -> case s of
        Nothing -> return ([], total)                   -- finished, nothing to store
        Just (Left x) -> goN (total+x)                  -- many Ns, we skip those
        Just (Right bs) -> do ao <- pushSepI total      -- chunk, start with gap, store chunk, continue
                              pushsI ao bs
                              goB total ao (total + S.length bs)

    goB !scoffs !arroffs !total = peekI >>= \s -> case s of
        Nothing -> return ([c], total)                  -- finished, return the single contig
        Just (Right bs) -> do pushsI arroffs bs         -- chunk, store and continue
                              goB scoffs arroffs (total + S.length bs)
        Just (Left x) -> do (cs,t) <- goN (total+x)     -- many Ns, we skip those and finish the contig
                            return (c:cs,t)
      where !c = Contig scoffs arroffs (total-scoffs)

pushSepI :: Int -> Iteratee a (ReaderT ScanEnv IO) Int
pushSepI pos = do se <- liftI ask
                  liftI (liftIO (pushVector (se_genome se) (se_inject se sep)))
                  vlen <- vec_length <$> liftI (liftIO (readIORef (se_genome se)))
                  reportI $ "starting contig at +" ++ showNum pos ++ " (^" ++ showNum vlen ++ ")"
                  return vlen

pushsI :: Int -> S.ByteString -> Iteratee a (ReaderT ScanEnv IO) ()
pushsI _arroffs s = liftI $ ask >>= \se -> liftIO $ forM_ (S.unpack s) $ \c ->
    case se_inject se c of 0 -> fail $ "unexpected code in FASTA input: " ++ show c
                           x -> pushVector (se_genome se) x

