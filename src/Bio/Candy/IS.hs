{-# LANGUAGE ForeignFunctionInterface #-}

-- | This is a thin wrapper around the SAIS algorithm, which I
-- "borrowed" from Bwa.  Apart from the obvious adapter, the only change
-- was to use an array of 40 bit integers for SA and subsamples instead
-- of the 32 bit ints in the original.
--
-- Indexing hg19 took 1h 30 min on a fairly beefy server.  Memory
-- consumption was about 46GB.  Substantial, but acceptable, at least
-- for the time being.  It doesn't scale to much larger genomes (say
-- over 4 gigabases), but that has to be enough for now.

module Bio.Candy.IS ( suffixArray ) where

import Bio.Prelude
import Foreign.C.Types          ( CLong(..), CInt(..) )
import Foreign.Marshal.Alloc    ( mallocBytes, free )

import Bio.Candy.Array

import qualified Data.ByteString        as S
import qualified Data.ByteString.Unsafe as S

-- Sanity check the SA?  Expensive, but great for debugging.
sanity_check :: Bool
sanity_check = False


{-# INLINE suffixArray #-}
suffixArray :: (String -> IO ()) -> IORef Vec -> CLong -> IO S.ByteString
suffixArray pr vec m = do
    pr' <- mk_callback $ \l -> pr $ "suffix array " ++ showNum (abs l) ++
                                    (if l < 0 then " final induction" else " initial induction")
    Vec len _ fp <- readIORef vec
    sarr' <- mallocBytes $ 5 * (len-2)
    withForeignPtr fp $ \p -> do
        sais_main pr' p sarr' 0 (fromIntegral $ len-3) (1+m) 1 >>= \r ->
            when (r < 0) $ fail $ "suffixArray: " ++ show r

        sa <- S.unsafePackCStringFinalizer sarr' (5 * (len-3)) (free sarr')

        when sanity_check $ do
            pr "sanity checking SA"
            when (any (\i -> let j = get40 sa 0 i in j < 0 || len-3 <= j) [0..len-4])
                $ fail "Oh fuck, some index points outside the string."

            let suff_compare !i !j | i == j = return EQ
                                   | otherwise = do
                    u <- peekElemOff p i
                    v <- peekElemOff p j
                    case u `compare` v of
                        LT -> return LT
                        GT -> return GT
                        EQ -> suff_compare (i+1) (j+1)

                sa_compare 0 = pr "all fine"
                sa_compare i = do
                    when (i `mod` 0x1000000 == 0) $ pr (showNum i)
                    r <- suff_compare (get40 sa 0 (i-1)) (get40 sa 0 i)
                    case r of
                        LT -> sa_compare (i-1)
                        _  -> fail "Oh fuck, the suffix array is not sorted."
            sa_compare (len-4)

        pr "suffix array done."
        return sa


foreign import ccall "sais_main"
    sais_main :: (FunPtr (CLong -> IO ())) -> Ptr Word8 -> Ptr Word8 -> CLong -> CLong -> CLong -> CInt -> IO CInt

foreign import ccall "wrapper"
    mk_callback :: (CLong -> IO ()) -> IO (FunPtr (CLong -> IO ()))
