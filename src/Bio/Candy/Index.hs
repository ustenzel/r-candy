{-# LANGUAGE ExistentialQuantification, FlexibleContexts, RecordWildCards #-}
module Bio.Candy.Index (
    BWIndex(..), createIndex, restoreIndex, readFileBS, hGetBS,
    Node(..), rootNode, children, isNull, huffFind,
    count, locate, display, decompress, arr_to_g,
    getBamRefs
                      ) where


import Bio.Bam.Header           ( BamSQ(..), Refs(..) )
import Bio.Prelude
import Data.Array.IArray        ( bounds, (!), array, listArray, elems )
import Data.Array.MArray        ( newArray, newArray_, writeArray, readArray )
import Data.Array.Unboxed       ( UArray )
import Data.Array.IO            ( IOUArray )
import Data.Array.Unsafe        ( unsafeFreeze )
import Data.Binary.Get          ( Get, runGet, bytesRead, skip, getWord8, getByteString )
import Data.Binary.Put          ( runPut, putWord8, putByteString )
import Data.ByteString.Internal ( create )
import Data.ByteString.Unsafe   ( unsafeUseAsCString )
import Foreign.Marshal.Alloc    ( allocaBytes )
import System.IO

import qualified Data.ByteString.Lazy           as L
import qualified Data.ByteString                as S
import qualified Data.IntMap                    as I
import qualified Data.Map                       as M
import qualified Data.Vector                    as V
import qualified Data.Vector.Unboxed            as U
import qualified Data.Vector.Algorithms.Intro   as U ( sortBy )

import Bio.Candy.Array
import Bio.Candy.Fasta
import Bio.Candy.Huffman
import Bio.Candy.IS
import Bio.Candy.Util

{-
$genome_layout

We read the input genome once, transcode it using a suitable Huffman
tree into the small local alphabet and store it into a dynamically
growing array.  The complement(!) of the genome is stored at the
beginning, then the reverse-complement of that (which is the reversed(!)
genome) is appended (plus three terminators, to enable the suffix array
construction).  Since we can only move backwards through the BWT, this
enables us to display or match the reverse-complement of the genome in
the first part and with natural coordinates, or the actual genome in the
second half with transformed coordinates.


$coordinate_system

We will always use half-open intervals and zero-based coordinates.
Offsets are thought to point "between" the bases.  On the forward strand
of the genome, this is completely natural and works just like in STL.
To get the reverse-complement of a region, the start and end just need
to be changed.  We can indicate any region as [start,end) with the
understanding that the region is on the forward strand if start < end,
on the reverse strand if end < start, and empty otherwise.  A position
in the genome is represented as (Right o) on the forward strand
(mnemonic: polymerase moves to the right) and (Left o) on the reverse
strand.  (Note that extracting one base from (Left o) does not give you
the complement of the base extractable from (Right o), it gives you the
one to its left!)


$suffix_array

Right now, there is no good way to construct a suffix array for the
double-stranded genome of ~6GB:

 * The algorithm following Larsson and Sadakane: "Faster Suffix Sorting"
   needs to store three arrays of at least 30GB each, totalling over
   96GB, which is impractical.

 * The skew algorithm needs various suffix arrays and subsamples from
   them, totalling over 100GB, also impractical.

 * Plain quicksort runs in under 40GB, but exhibits quadratic behaviour
   and therefore doesn't terminate in finite time.

 * SA-IS (stolen from BWA) appears to need little more than the suffix
   array and the input string, which could be less than 40GB.  Right now
   the implementation is too naive and wants 50GB.

 * BWT-SW (from Hon et. al.: "Constructing Compressed Suffix Arrays with
   large Alphabets") looks promising, but I don't understand it.

 * BWT-IS (from Okanohara and Sadakane: "A Linear-Time Burrows-Wheeler
   Transform using Induced Sorting") looks even more promising, but I
   don't understand it either.

On top of that, operating in external memory makes everything dog slow
and should be avoided at all cost.



$index_architecture

The R-Candy index is basically the original \"Alphabet Friendly FM index\"
(BWT and Wavelet-Tree), but we don't aim for the H_k space bound and use
the simpler version (without partitioning the BWT) that achieves H_0
space.  We use an explicit marker character, chiefly to avoid having to
store a sparse bit vector.

We can^W should actually get rid of the marker character, by storing a
separate bit vector of markers.  That only makes sense if we have a bit
vector B that allows fast (preferably O(1)) lookup of B(i) and answers
rank_1(i) at least whenever B(i)=1 while being compact for sparse
vectors.  S. Vigna has an implementation of such a structure; it is
slower than the dense version, but we could easily afford that, as it's
only used in locate() queries.  The sparse set is represented by storing
the low bits explicitly and the high bits in the Elias-Fano
representation for monotone sequences.  A rank query involves one
select_0 query on the high bits, then a linear scan through the (on
average very short) block found this way.  We need a fast select
implementation for that, which I don't understand well enough (yet).

Regarding the implementation of the rank-structure, Rank9 is the obvious
candidate with a very simple implementation and just 25% overhead.  If
SSE3 allows suitable 128 bit manipulations, we might think about using
Rank16 instead, at 12.6% overhead.


$todo

 * The reading of input is much more ugly than anticipated.  Some sort of
   RegEx or parser combinators would really be a boon.  It's also slower
   than necessary, but it's hardly clear if making it faster is possible
   without making it even uglier.

-}

-- $rankselect
-- This is an implementation similar to "Rank9" by Sebastiano Vigna.  We
-- split the bit vector into blocks of 8 words with 8 bytes each.  Each
-- block is prepended by an inventory (total from beginning of bit
-- vector) in one word and 7 subinventories in another word.  We store
-- subinventory k for k=0..7 in bits 63-9k .. 69-9k; since subinventory
-- 0 is always 0, this fits nicely.  The block itself follows after the
-- subinventories, everything is word-aligned and we can do popcounting
-- over one word.  Within words, bits are placed in native order.  The
-- last block is padded and another total is appended to avoid fence
-- post errors and special casing.

-- | Creates a wavelet, looking only at range (l..r), writes a 1 for
-- every symbol no less than k

mkWavelet :: UArray Word64 Word8 -> Word8 -> Word8 -> Word8 -> Handle -> IO ()
mkWavelet bwt l k r h = allocaBytes 80 $ \p -> toRank9 h p . toWords . toBits l k r $ elemsS bwt
    -- note: block size is 8 words and two header words, each word has
    -- 64 bits, hence 80 bytes in total

toRank9 :: Handle -> Ptr Word64 -> Stream Word64 -> IO ()
toRank9 h p (Stream next s0) = new_block 0 s0
  where
    new_block !total s = do pokeElemOff p 0 total ; fill_block total 0 0 0 s

    fill_block !total !subtotals !_   8 s = do pokeElemOff p 1 subtotals
                                               hPutBuf h p 80
                                               new_block total s

    fill_block !total !subtotals !ac !n s = case next s of
        Skip s' -> fill_block total subtotals ac n s'

        Done | n == 0    -> do pokeElemOff p 1 0    -- subtotal of sentinel block
                               hPutBuf h p 16

             | otherwise -> do pokeElemOff p 1 subtotals
                               mapM_ (\i -> pokeElemOff p (2+i) 0) [n..7]
                               hPutBuf h p 80
                               new_block total s

        Yield s' w -> do pokeElemOff p (2+n) w
                         fill_block total' subtotals' ac' (n+1) s'
          where !c          = popcount w
                !total'     = total + c
                !ac'        = ac + c
                !subtotals' = subtotals .|. (ac `shiftL` (63-9*n))


-- -------------
-- This is the old implementation of toRank9, which should yield
-- identical results.  It doesn't... and I'm not sure why.  Strangely,
-- both seem to work, so I don't know which one is wrong...

{-
toRank9Alt :: Stream Word64 -> L.ByteString
toRank9Alt = runPut . putRankSelect 0 . toBlocks . unstream

putRankSelect :: Word64 -> [[Word64]] -> Put
putRankSelect total [] = putWord64host total >> putWord64host 0
putRankSelect total (b:bs) = do putWord64host total
                                putWord64host subtotals
                                mapM_ putWord64host b
                                putRankSelect total' bs
    where
        (subtotals, total')     = accum 0 63 0 total b
        accum !st    _   _ !t [    ] = (st, t)
        accum !st  !sw !mt !t (w:ws) = accum st' (sw-9) (mt+c) (t+c) ws
          where c = popcount w
                st' = st .|. (mt `shiftL` sw)

toBlocks :: [Word64] -> [[Word64]]
toBlocks ws = case splitAt 8 ws of (u,v) | null v    -> [take 8 (u ++ repeat 0)]
                                         | otherwise -> u : toBlocks v

unstream :: Stream a -> [a]
unstream (Stream next s0) = go s0
  where
    go s = case next s of Yield s' a -> a : go s'
                          Skip s'    -> go s'
                          Done       -> []
-}
-- -------------

{-# INLINE toWords #-}
toWords :: Stream Bool -> Stream Word64
toWords (Stream next s0) = Stream go (Just (0,0,s0))
  where
    go Nothing = Done
    go (Just (!n,!a,s)) = case next s of
            Done -> Yield Nothing a
            Skip s' -> Skip (Just (n,a,s'))
            Yield s' b | n == 64   -> Yield (Just (  1, if b then 1            else 0, s')) a
                       | otherwise -> Skip  (Just (n+1, if b then a `setBit` n else a, s'))

{-# INLINE toBits #-}
toBits :: Word8 -> Word8 -> Word8 -> Stream Word8 -> Stream Bool
toBits l k r (Stream next s0) = Stream go s0
  where
    go s = case next s of Yield s' a | l <= a && a <= r -> Yield s' (a >= k)
                                     | otherwise        -> Skip s'
                          Skip s'                       -> Skip s'
                          Done                          -> Done

{-# INLINE elemsS #-}
elemsS :: UArray Word64 Word8 -> Stream Word8
elemsS myarr = let (l,r) = bounds myarr in Stream (go r) l
  where
    go r l = if l > r then Done else Yield (l+1) (myarr ! l)


data Stream a = forall s . Stream (s -> Step a s) s
data Step a s = Yield !s a | Skip !s | Done


-- | Counts how many bits are set in a 64 bit word.  Broadword algorithm,
-- nobody is expected to understand this at a glance.
{-# INLINE popcount #-}
popcount :: Word64 -> Word64
popcount !x = (w * 0x0101010101010101) `shiftR` 56
  where
    !y = x - (x .&. 0xAAAAAAAAAAAAAAAA) `shiftR` 1
    !z = (y .&. 0x3333333333333333) + ((y `shiftR` 2) .&. 0x3333333333333333)
    !w = (z + (z `shiftR` 4)) .&. 0x0F0F0F0F0F0F0F0F



-- | Create an index and immediately encode it.  The result is written
-- directly to a file (avoids annoying memory leaks).

createIndex :: (String -> IO ()) -> S.ByteString -> Genome -> IORef Vec
            -> HuffTree () () Word8 -> Int -> Handle -> IO ()
createIndex pr gname genome arr0 tree steps h = do
    pr "counting histogram"
    hist <- mkhist (maxSymbol tree) arr0
    pr $ show hist

    let header = runPut $ do putByteString "FMI\1"            -- magic
                             putWaveletTree tree
                             putString gname
                             putContigTable genome
                             putWord steps

        -- Write outline of wavelet tree, which just contains the code
        -- mapping (for later double checking) along with count of
        -- characters (from histogram) for computation of array sizes and
        -- construction of C array
        putWaveletTree (Leaf w c) | c /= 0 = do putWord8 c ; putWord (hist ! w)
        putWaveletTree (Branch _ _ _ t1 t2) = do putWord8 0 ; putWaveletTree t1 ; putWaveletTree t2
        putWaveletTree (Leaf _ _) = error "NUL in transcoded input"

    pr "writing header"
    L.hPut h header
    hlen <- return $! L.length header
    sarr <- suffixArray pr arr0 (fromIntegral $ maxSymbol tree)

    let bnds  = S.length sarr `div` 5
        hlen' = ((bnds + steps -1) `div` steps) * 5 + fromIntegral hlen
    pr $ "writing suffix array (" ++ showNum bnds ++ ") sample"

    mapM_ (\i -> S.hPut h $ S.take 5 $ S.drop (5*i) sarr) [0, steps .. bnds-1]
    S.hPut h $ S.replicate (7 - (hlen'-1) `mod` 8) 0
    pr $ showNum (length [0, steps .. bnds-1]) ++ " SA samples"

    pr "reading off BWT"
    -- About the terminator:  a real terminator is added, but the IS
    -- algorithm does not generate an actual suffix starting with the
    -- terminator.  Therefore, when building the BWT, we can pretend
    -- that the terminator was never there.
    bwt <- do Vec _ _ fp <- readIORef arr0
              bwt' <- newArray_ (0,fromIntegral bnds-1) :: IO (IOUArray Word64 Word8)
              withForeignPtr fp $ \p ->
                let bwt_loop !i | i == fromIntegral bnds = unsafeFreeze bwt'
                    bwt_loop !i = do when ((i+1) `mod` 2^(30::Int) == 0) . pr $ "..." ++ showNum i
                                     let o = fromIntegral $ get40 sarr 0 $ fromIntegral i
                                     c <- peekByteOff p $ fromIntegral $ if o == 0 then bnds-1 else o-1
                                     writeArray bwt' i c
                                     bwt_loop $! i+1
                in bwt_loop 0

    hist' <- mkhist' (maxSymbol tree) bwt
    pr $ "double checking histogram: "
      ++ if hist == hist' then "OK" else "fail"

    let mkwv (Leaf _ _) = return ()   -- nothing to store for leaves
        mkwv (Branch k _ _ l r) = do    -- pre-order traversal
            pr $ shows (minSymbol l) ".." ++ shows k ".." ++ show (maxSymbol r)
            mkWavelet bwt (minSymbol l) k (maxSymbol r) h
            mkwv l
            mkwv r

    pr "writing wavelets"
    mkwv tree
    pr "finished"


mkhist :: Word8 -> IORef Vec -> IO (UArray Word8 Word64)
mkhist mx v = do
    myarr <- newArray (0,mx) 0 :: IO (IOUArray Word8 Word64)
    Vec l _ fp <- readIORef v
    withForeignPtr fp $ \p ->
        let go !i | i == l = unsafeFreeze myarr
            go !i = do c <- peekByteOff p i
                       n <- readArray myarr c
                       writeArray myarr c $! n+1
                       go $! i+1
        in go 0


mkhist' :: Word8 -> UArray Word64 Word8 -> IO (UArray Word8 Word64)
mkhist' mx v = do
    myarr <- newArray (0,mx) 0 :: IO (IOUArray Word8 Word64)
    writeArray myarr 0 3
    let (0,l) = bounds v
        go !i | i > l = unsafeFreeze myarr
        go !i = do let c = v ! i
                   n <- readArray myarr c
                   writeArray myarr c $! n+1
                   go $! i+1
    go 0


-- | An actual full text index...
-- Note:  We read the contig table and all sorts of auxilliary
-- information from disk, then construct translation tables in memory.
-- Those are deliberately lazy, so they are only constructed when first
-- accessed.  Many client programs won't need all the tables.

data BWIndex = BWIndex {
    bwi_c :: {-# UNPACK #-} !(UArray Word8 Int),    -- ^ C array for LF-mapping
    bwi_total :: {-# UNPACK #-} !Int,               -- ^ total length (to get root node)
    bwi_sep :: {-# UNPACK #-} !Word8,               -- ^ code for gap character
    bwi_steps :: {-# UNPACK #-} !Int,               -- ^ sample step size
    bwi_genome_name:: {-# UNPACK #-} !S.ByteString, -- ^ arbitrary name

    bwi_genome :: [ Scaffold ],                     -- ^ scaffold list
    bwi_scaffolds :: M.Map S.ByteString Scaffold,   -- ^ scaffolds by name
    bwi_arr_to_g :: I.IntMap Scaffold,              -- ^ mapping from array coordinates to scaffolds
                                                    --   (deliberately lazy)
    bwi_sa_sample_ :: !S.ByteString,                -- ^ suffix array, sampled at gap positions
    bwi_inv_sa_sample :: U.Vector (Int,Int),        -- ^ mapping from array to SA coordinates
                                                    --   (deliberately lazy)
    bwi_wavelet_tree :: WaveletTree }               -- ^ actual BWT


bwi_sa_sample :: BWIndex -> Int -> Int
bwi_sa_sample bwi i = get40 (bwi_sa_sample_ bwi) 0 i

-- | Our wavelet tree is Huffman shaped (to achieve compression), we
-- store one rank/select structure in each internal node and one Word8
-- in each leaf.  The latter is used to double check if the trees of the
-- index and aligner are compatible (merely an optimization that saves
-- the aligner from having to do silly lookups).
type WaveletTree = HuffTree S.ByteString Int Word8

restoreIndex :: S.ByteString -> BWIndex
restoreIndex s = flip runGet (L.fromChunks [s]) $ do
    "FMI\1" <- getByteString 4
    ptree <- relabel `fmap` getPreWaveletTree
    bwi_genome_name <- getString
    bwi_genome <- getContigTable
    bwi_steps <- getWord
    let bwi_total = sum [ fromIntegral n | (_,(_,n)) <- huffAssocs ptree ] :: Int
    bwi_sa_sample_ <- getArraySlice $ (bwi_total + bwi_steps -1) `div` bwi_steps
    p0 <- fromIntegral `fmap` bytesRead

    let bwi_arr_to_g = I.fromList [ (fromIntegral $ co_offset_in_array c,sc)
                                  | sc <- bwi_genome
                                  , let (_,c) = I.findMin $ sc_contigs sc ]

        bwi_inv_sa_sample = U.create (do v <- U.unsafeThaw $ U.generate (1 + (bwi_total-1) `div` bwi_steps)
                                                           $ \i -> (get40 bwi_sa_sample_ 0 i, i * bwi_steps)
                                         U.sortBy (comparing fst) v
                                         return v)

        bwi_wavelet_tree = snd $ make_wavelets (p0 + (7 - (p0 -1) `mod` 8)) (snd $ totals ptree)

        bwi_scaffolds = M.fromList [ (sc_name sc, sc) | sc <- bwi_genome ]
        bwi_c = mkCArray ptree
        bwi_sep = injectTable (fmap fst ptree) sep

    return $! BWIndex{..}

  where
    getArraySlice r = do
        p <- fromIntegral `fmap` bytesRead
        skip $ r * 5
        return . S.take (r*5) $ S.drop p s

    -- get the outline of a wavelet tree, but not the wavelets themselves
    getPreWaveletTree = do c <- getWord8
                           if c /= 0 then do n <- getWord :: Get Int
                                             return $ leaf (c, n)
                                     else do t1 <- getPreWaveletTree
                                             t2 <- getPreWaveletTree
                                             return $ branch t1 t2

    totals (Leaf w (c,n)) = (n, Leaf w c)
    totals (Branch w () () l r) = let (n1, l') = totals l
                                      (n2, r') = totals r
                                  in (n1+n2, Branch w () (n1+n2) l' r')


    mkCArray ptree =
        let c1 = array (minSymbol ptree, maxSymbol ptree) [ (w,fromIntegral n) | (w,(_,n)) <- huffAssocs ptree ]
        in listArray (minSymbol ptree, maxSymbol ptree) (scanl (+) 0 (elems c1)) `asTypeOf` c1

    -- We need to compute the size of a wavelet.  The size is 16 (the
    -- two counts after the last block) plus 10*8*number of blocks.
    -- Each block contains 8*64==512 bits.  Since we know that every
    -- wavelets ends with a 64bit zero (the last subtotals), we can use
    -- that to double check.
    make_wavelets p (Leaf w c) = (p, Leaf w c)                  -- no wavelet for singleton
    make_wavelets p (Branch w _t1 t2 l r) =                     -- pre-order reconstruction
        let sz = ((fromIntegral t2 + 511) `div` 512) * 80 + 16  -- number of 64-byte blocks
            p1 = p+sz
            (p2, l') = make_wavelets p1 l
            (p3, r') = make_wavelets p2 r
            total'   = accursedUnutterablePerformIO (unsafeUseAsCString s (\a -> peek (a `plusPtr` (p+sz-16)))) :: Word64
            term'    = accursedUnutterablePerformIO (unsafeUseAsCString s (\a -> peek (a `plusPtr` (p+sz-8))))
            init'    = accursedUnutterablePerformIO (unsafeUseAsCString s (\a -> peek (a `plusPtr`  p)))
        in assert' ("initial total for " ++ shows w " is zero") (init' == (0 :: Word64)) $
           assert' ("final subtotal for " ++ shows w " is zero total is " ++ showNum total') (term' == (0 :: Word64)) $
           (p3, Branch w s p l' r')


-- | The "occ1 function" of FM index fame...
-- @occ1 s p i@ returns the number of set bits before(!) position i in
-- the wavelet stored at offset p in bytestring s.  (We use zero based
-- counting everywhere, much the same way as S.Vigna.)  In particular,
-- @occ1 s p 0@ is 0, @occ1 s p 1@ may be 0 or 1, and @occ1 s p l@ where
-- l is the total length of the wavelet gives the total number of set
-- bits in that wavelet.
occ1 :: S.ByteString -> Int -> Int -> Int
occ1 s off = \i -> accursedUnutterablePerformIO $ unsafeUseAsCString s $ \p0 -> do
    let p = p0 `plusPtr` off
        (!bnum, !bminor) = i `divMod` 512
        (!wnum, !wminor) = bminor `divMod` 64

    maj_inv <- peek . plusPtr p $ bnum * 80
    min_inv <- peek . plusPtr p $ bnum * 80 + 8
    word    <- peek . plusPtr p $ bnum * 80 + 16 + 8 * wnum

    let min_inv' = (min_inv `shiftR` (63 - 9*wnum)) .&. 0x1FF
        pop_inv' = popcount (word .&. (0x7FFFFFFFFFFFFFFF `shiftR` (63-wminor)))
        r = maj_inv + min_inv' + pop_inv' :: Word64

    return $! fromIntegral r


-- | Representation of a Node in the suffix tree.  This may not be the
-- final representation, but here it goes: @Node l r@ is the range [l,r)
-- of the suffix array represented by the appropriate index.  We count
-- lines from 0, and we use half-open intervals.  In particular, the
-- root node of @bwi@ is @Node 0 (bwi_total bwi)@, and @Node x x@ is an
-- empty node.
data Node = Node !Int !Int deriving (Eq, Ord)

instance Show Node where
    showsPrec _ (Node l r) = (++) (concat ["[", showNum l, "..", showNum r, ")"])

isNull :: Node -> Bool
isNull (Node l r) = assert' "node is non-degenerate" (l <= r) (l == r)

rootNode :: BWIndex -> Node
rootNode bwi = Node 0 (bwi_total bwi)

-- | finds all children of a node.  Must not be called on an empty
-- node, may return empty nodes in the resulting tree.
children :: BWIndex -> Node -> HuffTree () () Node
children bwi (Node l r) | l >= r    = error "children called for invalid node"
                        | otherwise = go l r (bwi_wavelet_tree bwi)
  where
    go u v (Leaf w _) = Leaf w (Node (bwi_c bwi ! w + u) (bwi_c bwi ! w + v))
    go u v (Branch k wvs wvo t1 t2) = let t1' = go (u - occ1 wvs wvo u) (v - occ1 wvs wvo v) t1
                                          t2' = go (occ1 wvs wvo u) (occ1 wvs wvo v) t2
                                      in Branch k () () t1' t2'


-- | Finds occurences of a pattern and counts them.  This function
-- performs plain search for a pattern.  A result of the form @Node l r@
-- represents @r-l@ matches.  Strictly speaking, this is the original
-- backward search algorithm, but since we reversed the genome at index
-- construction time, it actually searches forwards, and since we stored
-- both strands of the genome, we automatically search both strands
-- without complementing anything here.
count :: BWIndex -> [Word8] -> Node
count bwi ws0 = go (rootNode bwi) ws0
  where
    go node _ | isNull node = node
    go node [    ]          = node
    go node (w:ws)          = go (huffFind w $ children bwi node) ws

huffFind :: Word8 -> HuffTree a b Node -> Node
huffFind x (Leaf w n) = if x == w then n else Node 0 0
huffFind x (Branch k _ _ l r) = if x >= k then huffFind x r else huffFind x l

-- | A position in coordinates meaningful for the genome index.
-- Consists of the scaffold, the contig within the scaffold, and the
-- position within the scaffold.

type Hit = ( Scaffold, Contig, Int )

-- | Translates a node into genome coordinates.
-- Given a node, this finds the locations (scaffold and offset) of the
-- represented matches.  This can be done directly for SA positions that
-- have been sampled.  For other positions, we take all children and
-- recurse, keeping track of the offset.
--
-- For sampled positions, we translate to the array coordinate, look up
-- the closest scaffold, then the closest contig within the scaffold.
-- The reported coordinate is the name of the scaffold, and the position
-- from the SA sample.  The contig start in the genomne array is added,
-- the contig start in the SA subtracted, the offset from tracing
-- children added.
--
-- Note that due to the way the search works, the reported coordinate is
-- (usually) the *end* of the match!

locate :: BWIndex -> Node -> [ Hit ]
locate bwi = xlate 0
  where
    xpand !o n@(Node !l !r)
        | l >= r    = []
        | otherwise = concat [ xlate (succ o) n' | (_,n') <- huffAssocs $ children bwi n ]

    xlate !o (Node !l !r)
        | l >= r    = []
        | v  < r    = xpand o (Node l v) ++ arr_to_g bwi w : xlate o (Node (v+1) r)
        | otherwise = xpand o (Node l r)
      where
        u = l + bwi_steps bwi - 1
        v = u - (u `mod` bwi_steps bwi)
        w = (bwi_sa_sample bwi (v `div` bwi_steps bwi) + o) `mod` bwi_total bwi

arr_to_g :: BWIndex -> Int -> Hit
arr_to_g bwi i | i < 0 || i > bwi_total bwi = error $ concat [ "locate: SA coordinate out of range: "
                                                             , showNum i, " (", showNum (bwi_total bwi), ")" ]
               | 2*i < bwi_total bwi        = arr_to_g'                  i  negate
               | otherwise                  = arr_to_g' (bwi_total bwi - i) id
  where
    arr_to_g' j k | co == co_offset_in_array c = ( s, c, k $ j - co + co_offset_in_scaffold c )
                  | otherwise                  = error "locate: oops."
        where
          Just (( _,s),_) = I.maxViewWithKey . fst $ I.split (succ j) (bwi_arr_to_g bwi)
          Just ((co,c),_) = I.maxViewWithKey . fst $ I.split (fromIntegral j + 1) (sc_inv_contigs s)


-- | Selectively decompresses the genome.
-- Given genome coordinates, translates to SA coordinates, then starts
-- decompression at that position.  Decompression is done lazily, so we
-- can extract any desired portion of the genome; in fact, we can wrap
-- around and extract both strands, even repeatedly if desired.  Note
-- that since we indexed both strands anyway, we don't need to
-- complement or reverse anything.
--
-- Finding the coordinate is a bit intricate:  First get the scaffold,
-- then get the correct contig in the scaffold (and abort if no such
-- thing exists), then find the closest SA sample downstream(!) and note
-- how far away it is.  Start decompression there and drop as many words
-- from the beginning as necessary.
--
-- XXX If we're asked to decompress from between contigs, do we want to
-- generate a bunch of Ns?
-- XXX Broken for reverse strand coordinates right now.

display :: BWIndex -> Position -> [Word8]
display bwi (Pos name offs)
    | offs < 0                           = []
    | co /= co_offset_in_scaffold contig = error "display: oops."
    | offs > co_length contig + co       = error "coordinate outside contig"
    | otherwise                          = res

  where scaffold         = fromMaybe (error $ "scaffold not found " ++ show name) . M.lookup name $ bwi_scaffolds bwi
        ((co, contig),_) = fromMaybe (error $ "contig not found for pos " ++ showNum offs) . I.maxViewWithKey .
                                fst $ I.split (offs + 1) (sc_contigs scaffold)

        o' = co_offset_in_array contig + offs - co
        eff_offset = if offs < 0 then o' - 1 else bwi_total bwi - o' - 1

        -- We get the SA sample for a line that starts with a symbol
        -- that cannot get extracted.  Therefore, the starting point
        -- must be strictly to the right of the desired position.  So we
        -- search upwards from the start position.
        -- [Note the corner case we avoided by accident: we need a
        -- sample at the very end of the genome so we don't need to wrap
        -- around.  The end happens to be the lexically smallest suffix
        -- (the separator followed by the virtual end marker), so it is
        -- always sampled.]

        (arr_pos, sa_pos) = bin_search (bwi_inv_sa_sample bwi) 0 (U.length $ bwi_inv_sa_sample bwi)
          where
            bin_search !vec !l !r
                | l >= r = if l == U.length vec then error $ "no suitable SA sample for " ++ showNum eff_offset
                                                else U.unsafeIndex vec l
                | eff_offset < fst (U.unsafeIndex vec m) = bin_search vec l m
                | otherwise                              = bin_search vec (m+1) r
              where
                m = (l+r) `div` 2

        extract node = (\((w,n):_) -> w : extract n) . filter (not . isNull . snd) .
                            huffAssocs $ children bwi node

        res = drop (fromIntegral arr_pos - eff_offset -1) . extract $
                    Node (fromIntegral sa_pos) (fromIntegral $ sa_pos + 1)

decompress :: BWIndex -> [Word8]
decompress bwi = go 0 1
  where
    go a b = w : (if u == 0 then [] else go u v)
      where ((w,Node u v):[]) = filter (not . isNull . snd) $ huffAssocs $ children bwi (Node a b)

-- | Reads an entire file into a @S.ByteString@.
-- This is equivalent to @S.readFile@, but calls @hGetBuf@ repeatedly so
-- that it can work on files bigger than 2GB.
readFileBS :: (Int -> IO ()) -> FilePath -> IO S.ByteString
readFileBS pr f = bracket (openBinaryFile f ReadMode) hClose
    (\h -> hFileSize h >>= hGetBS pr h . fromIntegral)

-- | Reads into a @S.ByteString@.
-- This is equivalent to @S.hGet@, but calls @hGetBuf@ repeatedly so
-- that it can work on inputs bigger than 2GB.
hGetBS :: (Int -> IO ()) -> Handle -> Int -> IO S.ByteString
hGetBS pr h i0 | i0 >  0    = create i0 $ \p -> get p i0
               | i0 == 0    = return S.empty
               | otherwise = error "negative file size"
  where
    get p i | i > 0 = do pr i
                         l <- hGetBuf h p (min i (2^(28::Int)))
                         if l <= 0 then error "read failed (EOF?)"
                                   else get (p `plusPtr` l) (i-l)
            | otherwise = return ()

assert' :: String -> Bool -> a -> a
assert' _ True a = a
assert' m False _ = error $ "assertion failed: " ++ m

getBamRefs :: BWIndex -> Refs
getBamRefs db = Refs $ V.fromList [ BamSQ (sc_name s) (sc_length s) [] | s <- bwi_genome db ]

