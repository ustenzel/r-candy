module Bio.Candy.Util (
    Word64, Word8, c2w, w2c, upCase, downCase,
    putWord, getWord, putList, getList, putString, getString,
    version, showNum
                     ) where

import Bio.Prelude
import Data.Binary.Get      ( Get, getWord8, getByteString )
import Data.Binary.Put      ( Put, putWord8, putByteString )
import Paths_r_candy        ( version )

import qualified Data.ByteString as S

-- | Very fast and very inaccurate conversion to upper case for
-- ByteString.  Use only on strings composed solely of letters or where
-- you don't care for other characters.
{-# INLINE upCase #-}
upCase :: Word8 -> Word8
upCase c = clearBit c 5

-- | Very fast and very inaccurate conversion to lower case for
-- ByteString.  Use only on strings composed solely of letters or where
-- you don't care for other characters.
{-# INLINE downCase #-}
downCase :: Word8 -> Word8
downCase c = setBit c 5

-- | Put a nonnegative integer in binary format.  A variable size
-- encoding is used.
putWord :: (Integral a, Bits a) => a -> Put
putWord x | x < 0 = error "unsupported: putting negative numbers"
putWord x = go x
  where
    go w | w < 0x80  = putWord8 $ fromIntegral w
         | otherwise = putWord8 (0x80 .|. fromIntegral w) >> go (w `shiftR` 7)

-- | Get a nonnegative integer from binary format.  A variable size
-- encoding is used.
getWord :: (Integral a, Bits a) => Get a
getWord = go 0 0
  where
    go !acc !sh = do w <- getWord8
                     let acc' = acc .|. (fromIntegral (w .&. 0x7F) `shiftL` sh)
                     if w < 0x80 then return $! acc'
                                 else go acc' (sh+7)

-- | Put a list into binary.  First puts the length in variable length
-- encoding, followed by the elements.
putList :: (a -> Put) -> [a] -> Put
putList f xs = do putWord $ length xs ; mapM_ f xs

-- | Get a list from binary.  First gets the length from variable length
-- encoding, then gets the elements.
getList :: Get a -> Get [a]
getList f = getWord >>= flip replicateM f

-- | Put a 'S.ByteString' into binary.  First puts the length, then the string.
putString :: S.ByteString -> Put
putString s = do putWord $ S.length s ; putByteString s

-- | Get a 'S.ByteString' from binary.  First gets the length, then the string.
getString :: Get S.ByteString
getString = getWord >>= getByteString

