{-# LANGUAGE ForeignFunctionInterface #-}

-- | Arrays of rather big (40 bit) integers.  Hardly anything in here
-- will make sense on a 32-bit machine, so don't even try it.

module Bio.Candy.Array (
    MArray(..), IArray(..), marray40, marray32, Word64, get40,
    MArray40, newArray40_, sizeArray40, readArray40, writeArray40, freezeMArray40,
    MArray32, newArray32_, sizeArray32, readArray32, writeArray32, freezeMArray32,
    Vec(..), newVector, pushVector, freezeVector, lengthVector, IORef, readIORef, writeIORef,
    accursedUnutterablePerformIO
                      ) where

import Bio.Prelude
import Data.ByteString as S     ( ByteString, index )
import Data.ByteString.Internal ( accursedUnutterablePerformIO, memcpy )

data IArray = IArray { size :: !Word64, index :: !(Word64 -> Word64) }

data MArray a = MArray {
        newArr :: Word64 -> IO a,
        sizeArr :: a -> Word64,
        readArr :: a -> Word64 -> IO Word64,
        writeArr :: a -> Word64 -> Word64 -> IO (),
        freeze :: a -> IO IArray }

-- | Compact storage of integers in the range 0..2^40-1.
-- Implemented as a C-array of bytes storing 5-byte words in little
-- endian format, it is probably slow, but quite compact.
data MArray40 = MArray40 !Word64 !(ForeignPtr Word8)

{-# INLINE newArray40_ #-}
newArray40_ :: Word64 -> IO MArray40
newArray40_ sz =  MArray40 sz <$> mallocForeignPtrBytes (fromIntegral $ 5*sz)

{-# INLINE sizeArray40 #-}
sizeArray40 :: MArray40 -> Word64
sizeArray40 (MArray40 !s _) = s

{-# INLINE readArray40 #-}
readArray40 :: MArray40 -> Word64 -> IO Word64
readArray40 (MArray40 !s !fp) !i
    | s <= i = fail "range error"
    | otherwise = let o = fromIntegral (5*i) in
                  withForeignPtr fp $ \p ->
                        peekByteOff p (o+0) >>= \a ->
                        peekByteOff p (o+1) >>= \b ->
                        peekByteOff p (o+2) >>= \c ->
                        peekByteOff p (o+3) >>= \d ->
                        peekByteOff p (o+4) >>= \e ->
                        return $! (fromIntegral (e::Word8) .&. 0xff) `shiftL` 32 .|.
                                  (fromIntegral (d::Word8) .&. 0xff) `shiftL` 24 .|.
                                  (fromIntegral (c::Word8) .&. 0xff) `shiftL` 16 .|.
                                  (fromIntegral (b::Word8) .&. 0xff) `shiftL`  8 .|.
                                  (fromIntegral (a::Word8) .&. 0xff)

{-# INLINE writeArray40 #-}
writeArray40 :: MArray40 -> Word64 -> Word64 -> IO ()
writeArray40 (MArray40 !s !fp) !i !e
    | s <= i = fail "range error"
    | otherwise = do let o = fromIntegral $ 5*i
                     withForeignPtr fp $ \p ->
                        pokeByteOff p (o+0) (fromIntegral $ e             .&. 0xff :: Word8) >>
                        pokeByteOff p (o+1) (fromIntegral $ e `shiftR`  8 .&. 0xff :: Word8) >>
                        pokeByteOff p (o+2) (fromIntegral $ e `shiftR` 16 .&. 0xff :: Word8) >>
                        pokeByteOff p (o+3) (fromIntegral $ e `shiftR` 24 .&. 0xff :: Word8) >>
                        pokeByteOff p (o+4) (fromIntegral $ e `shiftR` 32 .&. 0xff :: Word8)

{-# INLINE freezeMArray40 #-}
freezeMArray40 :: MArray40 -> IO IArray
freezeMArray40 a@(MArray40 !s _) = return $! IArray s (accursedUnutterablePerformIO . readArray40 a)


-- | Compact storage of integers in the range 0..2^32-1.  This is
-- implemented as a C array of 4-byte integers, so should be a lot
-- faster, but cannot count through the whole genome on both strands.
data MArray32 = MArray32 !Word64 !(ForeignPtr Word32)

{-# INLINE newArray32_ #-}
newArray32_ :: Word64 -> IO MArray32
newArray32_ sz =  MArray32 sz <$> mallocForeignPtrArray (fromIntegral sz)

{-# INLINE sizeArray32 #-}
sizeArray32 :: MArray32 -> Word64
sizeArray32 (MArray32 !s _) = s

{-# INLINE readArray32 #-}
readArray32 :: MArray32 -> Word64 -> IO Word64
readArray32 (MArray32 !s !fp) !o
    | o < 0 || s <= o = fail "range error"
    | otherwise = withForeignPtr fp $ \p -> fromIntegral <$> peekElemOff p (fromIntegral o)

{-# INLINE writeArray32 #-}
writeArray32 :: MArray32 -> Word64 -> Word64 -> IO ()
writeArray32 a@(MArray32 !s !fp) !o !e
    | o < 0 || s <= o = fail "range error"
    | otherwise = do withForeignPtr fp $ \p -> pokeElemOff p (fromIntegral o) (fromIntegral e)
                     f <- readArray32 a o
                     unless (f == e) $ fail $ "couldn't store " ++ show e
                                           ++ " got " ++ show f ++ " instead"

{-# INLINE freezeMArray32 #-}
freezeMArray32 :: MArray32 -> IO IArray
freezeMArray32 a@(MArray32 !s _) = return $! IArray s (accursedUnutterablePerformIO . readArray32 a)


{-# INLINE marray40 #-}
marray40 :: MArray MArray40
marray40 = MArray newArray40_ sizeArray40 readArray40 writeArray40 freezeMArray40

{-# INLINE marray32 #-}
marray32 :: MArray MArray32
marray32 = MArray newArray32_ sizeArray32 readArray32 writeArray32 freezeMArray32


-- | a dynamically growing array of bytes
data Vec = Vec { vec_length :: {-# UNPACK #-} !Int
               , vec_capacity :: {-# UNPACK #-} !Int
               , vec_store :: {-# UNPACK #-} !(ForeignPtr Word8) }

{-# INLINE newVector #-}
newVector :: IO (IORef Vec)
newVector = do fp <- mallocForeignPtrBytes 4096
               newIORef $! Vec 0 4096 fp

{-# INLINE lengthVector #-}
lengthVector :: IORef Vec -> IO Int
lengthVector v = vec_length <$> readIORef v

{-# INLINE pushVector #-}
pushVector :: IORef Vec -> Word8 -> IO ()
pushVector v w = do Vec len cap fp <- readIORef v
                    if cap > len then do withForeignPtr fp $ \p -> pokeElemOff p len w
                                         writeIORef v $! Vec (succ len) cap fp
                                 else do fp' <- mallocForeignPtrBytes (2*cap)
                                         withForeignPtr fp' $ \dest ->
                                          withForeignPtr fp  $ \source ->
                                           memcpy dest source (fromIntegral len) >>
                                           pokeElemOff dest len w
                                         writeIORef v $! Vec (succ len) (2*cap) fp'

{-# INLINE freezeVector #-}
freezeVector :: IORef Vec -> IO IArray
freezeVector v = do Vec l _ fp <- readIORef v
                    return $! IArray (fromIntegral l) (\i ->
                        fromIntegral $ accursedUnutterablePerformIO $
                            withForeignPtr fp $ \p -> peekElemOff p $ fromIntegral i)


-- treat ByteString as array of 40 bit integers
{-# INLINE get40 #-}
get40 :: S.ByteString -> Int -> Int -> Int
get40 s p = \x ->
  let j = fromIntegral (s `S.index` (p+5*x+4)) :: Int8 -- propagate sign
  in fromIntegral (s `S.index` (p+5*x+0)) `shiftL` 0 .|.
     fromIntegral (s `S.index` (p+5*x+1)) `shiftL` 8 .|.
     fromIntegral (s `S.index` (p+5*x+2)) `shiftL` 16 .|.
     fromIntegral (s `S.index` (p+5*x+3)) `shiftL` 24 .|.
     fromIntegral j `shiftL` 32

