{-# LANGUAGE FlexibleContexts, RecordWildCards, CPP #-}
import Bio.Bam
import Bio.Candy.Array
import Bio.Candy.Fasta
import Bio.Candy.Huffman
import Bio.Candy.Index
import Bio.Candy.Util
import Bio.Prelude
import Bio.Streaming.Vector            ( stream2vectorN )
import Bio.TwoBit
import Control.Monad.Log
import System.Console.GetOpt
import System.IO
import System.Random
import System.Time

import qualified Data.ByteString       as B
import qualified Data.ByteString.Char8 as S
import qualified Data.IntMap.Strict    as I
import qualified Data.Vector.Generic   as V
import qualified Data.Vector.Storable  as W
import qualified Data.Vector.Unboxed   as U
import qualified Streaming.Prelude     as Q

{-
This is R-Candy (Rocket Candy), a cheap fuel good for blast off, but
only for small rockets.

So, we read a genome index, then stream BAM files.  Each read is treated
independently, forget about paired end reads.  Search is straight
forward Branch-and-Bound, the score takes deamination and divergence
into account.

Output is a tough question.  We probably need to output more than one
alignment, and a score, too.  To this end, each alignment becomes its
own BAM record, and the score goes into AS:f.  In a sane world, the
output might not be BAM.

Note on input and output:  if input is BAM and output is BAM, I
cannot possibly clean out all meaningless tagged fields, because there
is no way to tell if they have meaning independent of an alignment.
Dirty output therefore is the sole responsibility of the user...
-}


options :: [OptDescr (Conf -> IO Conf)]
options = [
    Option "o" ["output"]                   (ReqArg set_output   "FILE")    "Write output to FILE",
    Option "G" ["database","genome"]        (ReqArg set_database "FILE")    "Use FILE as genome database",
    Option "A" ["align-score", "limit"]     (ReqArg set_limit   "SCORE")    "Set search limit to SCORE (default 12)",
    Option "l" loverhang_long               (ReqArg set_lparam   "PROB")    "Parameter for 5' overhang is PROB",
    Option "r" ["right-overhang-param"]     (ReqArg set_rparam   "PROB")    "Parameter for 3' overhang is PROB, assume single-strand prep",
    Option "d" delta_long                   (ReqArg set_ds_deam  "FRAC")    "Deamination rate in double stranded section is FRAC",
    Option "s" sigma_long                   (ReqArg set_ss_deam  "FRAC")    "Deamination rate in single stranded section is FRAC",
    Option "D" ["divergence"]               (ReqArg set_div      "FRAC")    "Set expected divergence to FRAC",
    Option "I" ["indel-rate"]               (ReqArg set_pindel   "FRAC")    "Set expected rate of indels to FRAC",
    Option "n" ["hits-limit"]               (ReqArg set_hlim      "NUM")    "Output up to NUM hits per read (64)",
    Option "c" ["count-limit"]              (ReqArg set_clim      "NUM")    "Count up to NUM hits per read for NH field (256)",
    Option ""  ["build"]                    (ReqArg set_build    "FILE")    "Build index and write it to FILE",
    Option "m" ["maxn"]                     (ReqArg set_maxn        "N")    "Set maximum number of Ns that is not a gap (default: 49)",
    Option "S" ["stepsize"]                 (ReqArg set_steps       "N")    "Set step size for suffix array sample (default: 67)",
    Option "g" ["genome-name"]              (ReqArg set_gname    "NAME")    "Set genome name to NAME",
    Option [ ] ["smoke-test"]               (ReqArg set_smoke    "2BIT")    "Run smoke test on genome 2BIT",
    Option "v" ["verbose"]                  (NoArg  set_verbose)            "Print more progress information",
    Option "q" ["quiet"]                    (NoArg  set_quiet)              "Print no progress information",
    Option "V" ["version"]                  (NoArg print_version)           "Print version number and exit",
    Option "h?" ["help", "usage"]           (NoArg print_help)              "Print usage information and exit" ]

  where
    loverhang_long = ["overhang-param","left-overhang-param"]
    delta_long     = ["deamination-rate","ds-deamination-rate","double-strand-deamination-rate"]
    sigma_long     = ["ss-deamination-rate","single-strand-deamination-rate"]

    set_output    f c = return $ c { conf_output = if f == "-" then pipeBamOutput else writeBamFile f }
    set_build     f c = return $ c { conf_main = build_main, conf_idxout = if f == "-" then \k -> k stdout else withFile f WriteMode }
    set_database  f c = B.readFile f >>= \i -> return $ c { conf_database  = restoreIndex i }
    set_smoke     f c = openTwoBit f >>= \i -> return $ c { conf_main      = smoke_test i }
    set_limit     a c = readIO' a    >>= \l -> return $ c { conf_limit     = l }
    set_lparam    a c = readIO' a    >>= \x -> return $ c { conf_lparam    = Just $ abs x / (1 + abs x) }
    set_rparam    a c = readIO' a    >>= \x -> return $ c { conf_rparam    = Just $ abs x / (1 + abs x) }
    set_ds_deam   a c = readIO' a    >>= \x -> return $ c { conf_ds_deam   = x }
    set_ss_deam   a c = readIO' a    >>= \x -> return $ c { conf_ss_deam   = x }
    set_div       a c = readIO' a    >>= \x -> return $ c { conf_div       = x / 3 }
    set_pindel    a c = readIO' a    >>= \x -> return $ c { conf_gap       = - log x }
    set_hlim      a c = readIO' a    >>= \x -> return $ c { conf_hit_limit = x }
    set_clim      a c = readIO' a    >>= \x -> return $ c { conf_cnt_limit = x }
    set_maxn      a c = readIO' a    >>= \n' -> return $ c { conf_se = (conf_se c) { se_num_n = n' } }
    set_steps     a c = readIO' a    >>= \s' -> return $ c { conf_steps = s' }
    set_gname     a c =                         return $ c { conf_gname = a }
    set_verbose     c =                         return $ c { conf_info = conf_progress c }
    set_quiet       c =                         return $ c { conf_se = (conf_se c) { se_report = \_ -> return () }}

    readIO' a = case reads a of [(x,"")] -> return x
                                _ -> fail $ "no parse: " ++ a

    print_version _ = do pn <- getProgName
                         hPutStr stderr $ pn ++ ", version " ++ showVersion version ++ "\n"
                         exitSuccess

data Conf = Conf { conf_database  :: BWIndex
                 , conf_output    :: BamMeta -> Stream (Of BamRec) LIO () -> LIO ()
                 , conf_main      :: Conf -> [FilePath] -> IO ()
                 , conf_limit     :: Double
                 , conf_lparam    :: Maybe Double
                 , conf_rparam    :: Maybe Double
                 , conf_ds_deam   :: Double
                 , conf_ss_deam   :: Double
                 , conf_div       :: Double
                 , conf_gap       :: Double
                 , conf_hit_limit :: Int
                 , conf_cnt_limit :: Int
                 , conf_se        :: ScanEnv
                 , conf_steps     :: Int
                 , conf_idxout    :: (Handle -> IO ()) -> IO ()
                 , conf_info      :: String -> IO ()
                 , conf_gname     :: String
                 , conf_progress  :: String -> IO () }

defaultConf :: IO Conf
defaultConf = do
    p <- getProgName
    l <- newMVar ()
    let show2 i cs = intToDigit (i `div` 10) : intToDigit (i `mod` 10) : cs
        progress   = \m -> do takeMVar l
                              time <- getClockTime >>= toCalendarTime
                              hPutStrLn stderr $ '[' : show2 (ctHour time) ":" ++ show2 (ctMin time) ":"
                                                    ++ show2 (ctSec time) "] " ++ p ++ ": " ++ m
                              putMVar l ()

    genome <- newVector
    return $! Conf { conf_database  = error "genome database not specified"
                   , conf_output    = \h -> pipeBamOutput h . protectTerm
                   , conf_main      = rcandy
                   , conf_limit     = 12 -- about two mismatches?
                   , conf_lparam    = Nothing
                   , conf_rparam    = Nothing
                   , conf_ds_deam   = 0.02
                   , conf_ss_deam   = 0.45
                   , conf_div       = 0.015 / 3 -- approx. human-chimp
                   , conf_gap       = log 100000
                   , conf_hit_limit = 64
                   , conf_cnt_limit = 256
                   , conf_steps     = 67
                   , conf_idxout    = \k -> k stdout
                   , conf_info      = \_ -> return ()
                   , conf_gname     = ""
                   , conf_progress  = progress
                   , conf_se = ScanEnv { se_genome = genome
                                       , se_num_n = 49
                                       , se_inject = xl_rc
                                       , se_report = progress }}
  where
    xl_rc  = protect 'N' $ injectTable unmaskedGenomeTree . defaultComplFunc . upCase
    protect x f = \c -> case f c of 0 -> f (c2w x) ; y -> y


print_help :: Conf -> IO Conf
print_help _ = do pn <- getProgName
                  let header = pn ++ " [OPTION...] [BAM-FILE...]\n" ++
                               pn ++ " --build [OPTION...] [FASTA-FILE...]"
                  putStr $ usageInfo header options
                  exitSuccess

main :: IO ()
main = do
    (opts, infiles, errors) <- getOpt Permute options <$> getArgs
    unless (null errors) $ do
        mapM_ putStrLn errors
        exitFailure

    conf <- foldl (>>=) defaultConf opts
    conf_main conf conf infiles

rcandy :: Conf -> [FilePath] -> IO ()
rcandy conf infiles = do
    add_pg <- addPG (Just version)
    withLogging_ (LoggingConf Warning Warning Error 10 True) $
        concatInputs infiles $ \bam_hdr ->
            conf_output conf (add_pg bam_hdr { meta_refs = getBamRefs (conf_database conf)})
            . Q.concat . Q.map (bam2bam conf . cleanInput)
            . progressGen (\n a -> unpack (b_qname (unpackBam a)) ++ ", "
                                   ++ showNum n ++ " records processed") 256

-- | Main function for building an index.
build_main :: Conf -> [FilePath] -> IO ()
build_main conf infiles = do
    let parseInput se "-" = parseGenomeHandle se stdin
        parseInput se fp  = parseGenomeFile se fp

    genome_meta <- if null infiles
                   then parseInput (conf_se conf) "-"
                   else concat <$> mapM (parseInput (conf_se conf)) infiles
    when (all (I.null . sc_contigs) genome_meta) (fail "no input sequence")

    flip mapM_ genome_meta $ \sc ->
        conf_info conf $ concat [ "Scaffold "
                                , show $ sc_name sc `S.append` sc_description sc, ": "
                                , show . I.size $ sc_contigs sc, " contigs, "
                                , showNum . sum . fmap co_length $ sc_contigs sc, "/"
                                , showNum $ sc_length sc, " bases" ]

    conf_info conf "creating reverse strand"
    appendSecondStrand (se_genome $ conf_se conf) mycompl
    conf_info conf "done"
    conf_idxout conf $
        createIndex (conf_info conf) (S.pack $ conf_gname conf) genome_meta
                    (se_genome $ conf_se conf) unmaskedGenomeTree (conf_steps conf)
  where
    mycompl     = protect 'N' $ injectTable unmaskedGenomeTree . defaultComplFunc . projectTable unmaskedGenomeTree
    protect x f = \c -> case f c of 0 -> f (c2w x) ; y -> y


-- | Repeatedly (100x) samples a random piece of sequence of a fixed
-- length (50) uniformly from the genome and prints them in SAM format.
-- Only pieces that are not hard masked are sampled, soft masking is
-- allowed and ignored.  On a 32bit platform, this will fail for genomes
-- larger than 2G bases.  However, if you're running this code on a
-- 32bit platform, you have bigger problems to worry about.
smoke_test :: TwoBitFile -> Conf -> [FilePath] -> IO ()
smoke_test tbf conf _ =
    pipeSamOutput (mempty::BamMeta) { meta_refs = getBamRefs (conf_database conf) }
    $ Q.concat $ Q.mapM enum1 $ each [1..100]
  where
    !total = fromIntegral $ sum $ fmap tbc_dna_size $ tbf_chroms tbf

    !frags = I.fromList $ zip (scanl (+) 0 $ map (fromIntegral . tbc_dna_size) $ toList $ tbf_chroms tbf)
                              (toList $ tbf_chroms tbf)

    !vv = V.fromListN 16 $ [8,2,1,4,8,2,1,4,15,15,15,15,15,15,15,15]

    enum1 :: Int -> IO [BamRec]
    enum1 i = do (rseq, pos, sq) <- getRandomSeq 50
                 return $! bam2bam conf nullBamRec
                            { b_qname = S.pack (show i)
                            , b_seq   = sq
                            , b_qual  = Just $ V.replicate (V.length sq) (Q 30)
                            , b_exts  = insertE "SC" (Text rseq) $ insertE "SP" (Int pos) [] }

    entropy :: Vector_Nucs_half Nucleotides -> Double
    entropy ns = sum [ fromIntegral c * log (fromIntegral (V.length ns) / fromIntegral c) / log 2
                     | c <- [ V.length (V.filter (== x) ns) | x <- [nucsA, nucsC, nucsG, nucsT] ]
                     , c /= 0 ]

    getRandomSeq :: Int -> IO (Bytes, Int, Vector_Nucs_half Nucleotides)
    getRandomSeq len = do
        p <- randomRIO (0, 2*total-1)
        let (o,s) = fst $ fromJust $ I.maxViewWithKey $ fst $ I.split (p+1) frags
        let sq    = runST $ stream2vectorN len $ Q.map Ns $ unpackRSWith vv maxBound $ tbc_fwd_seq s (p-o)

        if p + len <= o + fromIntegral (tbc_dna_size s) &&
           V.all isProperBase sq &&
           2 * entropy sq > fromIntegral (V.length sq)
        then pure (tbc_name s, p-o, sq)
        else getRandomSeq len


-- | recursive alignment with aDNA score
-- We search recursively, using Branch-And-Bound.  The initial bound is
-- the limit of what we're willing to output, after we find something,
-- the bound is the current best score plus a little bit to get sensible
-- MAPQ.

type Score = Double
type Probs = U.Vector Score -- one score-equivalent for each symbol(!)
type Match = (Node, Score, [Op]) -- urgh, ugly :(

data Op = I | D Word8 | M Word8 -- even uglier...

-- This is B&B, we're going for the first hit only.  If needed, we can
-- build another limited DFS to look for more.  I don't want to burden
-- this search with too much adjustement of the limit.
--
-- XXX  Repeatedly looking for gaps is silly; the search becomes
-- cheaper if we don't bother after the first gap anymore.
align :: Conf -> [Probs] -> [Match]
align Conf{..} = go conf_limit 0 [] (rootNode conf_database) (const [])
  where
    prj = projectTable (bwi_wavelet_tree conf_database)

    -- lookahead = 40 -- another one for MAPQ?

    -- (Does this want to be a monad, maybe?)
    -- go :: score -> score -> [MdOp] -> Node -> (score -> [Match]) -> [Probs] -> [Match]
    go !lim !score  _ !nd k  _ | score > lim || isNull nd = k lim
    go !lim !score md !nd k [] = (nd, score, md) : k (min lim score)

    go !lim !score md !nd k (pp:pps) = go' lim $ sortBy (comparing fst) $
        ( conf_gap, \l' s' k' -> go l' s' (I:md) nd k' pps ) :                    -- Insertion
        [ r | (sym,nd') <- huffAssocs $ children conf_database nd
            , not (isNull nd')
            , let difscore = V.unsafeIndex pp (fromIntegral sym)
            , difscore > 0
            , r <- [ ( difscore, \l' s' k' -> go l' s' (M (prj sym):md) nd' k' pps )            -- match or mismatch
                   , ( conf_gap, \l' s' k' -> go l' s' (D (prj sym):md) nd' k' (pp:pps) ) ] ]   -- deletion
      where
        go' l [          ] = k l
        go' l ((ds,k'):xs) =
            let s' = score + ds                 -- new score
            in if s' > l then k l               -- if bad enough, rest of our list is unneeded
               else k' l s' (\l' -> go' l' xs)   -- recurse

-- We have to turn a BAM record into a matrix of probabilities.  For
-- each base $b$,  we compute $P(b|r)$ where $r$ is the *symbol* in the
-- reference.  We store the negative log of probabilities, which will be
-- a positive number, or a negative number if the probability wouldn't
-- make sense.

bam_to_pps :: BWIndex -> DModel -> BamRec -> [Probs]
bam_to_pps BWIndex{..} dm br = zipWith3 mkprobs [0..] (V.toList $ b_seq br) (maybe (repeat (Q 23)) V.toList $ b_qual br)
  where
    prj = w2c . projectTable bwi_wavelet_tree
    len = V.length $ b_seq br
    mkprobs i (Ns n) (Q q) = V.fromListN (fromIntegral $ maxSymbol bwi_wavelet_tree + 1)
                                [ negate $ p n (prj sym) | sym <- [ 0 .. maxSymbol bwi_wavelet_tree ] ]
      where
        (dv, dc, dg) = dm i (len-i-1)
        eps_ = 0.25 * 10 ** (-fromIntegral q*0.1)
        eps = eps_ + dv - eps_ * dv

        p 1 'A' = log $ 1 - 3*eps
        p 1 'C' = log $ eps
        p 1 'G' = log $ eps + dg - 4*dg*eps
        p 1 'T' = log $ eps

        p 2 'A' = log $ eps
        p 2 'C' = log $ 1 - 3*eps - dc + 4*dc*eps
        p 2 'G' = log $ eps
        p 2 'T' = log $ eps

        p 4 'A' = log $ eps
        p 4 'C' = log $ eps
        p 4 'G' = log $ 1 - 3*eps - dg + 4*dg*eps
        p 4 'T' = log $ eps

        p 8 'A' = log $ eps
        p 8 'C' = log $ eps + dc - 4*dc*eps
        p 8 'G' = log $ eps
        p 8 'T' = log $ 1 - 3*eps

        p _ 'N' = log $ 0.25
        p _  _  = 1.0

-- | Aligns a read and encodes the output... somehow.  Somewhat
-- arbitrarily, we discard any more than 64 hits and instead report
-- \"unaligned\" and the number of hits.  If there are more than 256
-- hits, we don't bother enumerating them and report \"lots\" instead.
-- For fewer than 64 hits, we try and compute sensible quality scores.

bam2bam :: Conf -> BamRec -> [BamRec]
bam2bam conf@Conf{..} qrec
    | no_hits   = [ qrec { b_flag  = b_flag qrec .|. flagUnmapped
                         , b_rname = invalidRefseq
                         , b_pos   = invalidPos
                         , b_mapq  = Q 0
                         , b_cigar = V.empty
                         , b_mrnm  = invalidRefseq
                         , b_mpos  = invalidPos
                         , b_isize = 0 } ]

    | otherwise = take conf_hit_limit
                  [ if forward then orec else flipRecord orec
                  | (node, scr, ecig) <- hits
                  , (scaffold, _, pos) <- locate conf_database node
                  , let forward = pos >= 0
                  , let the_md = toMd forward (show $ V.toList $ b_seq frec) ecig
                  , let orec = frec { b_flag  = b_flag frec .&. complement flagUnmapped
                                    , b_rname = Refseq $ fromIntegral $ sc_num scaffold
                                    , b_pos   = if forward then pos - sum (map aln_length ecig) else -pos
                                    , b_mapq  = Q . round . negate $ 10 * log1p (- exp (total_score - scr)) / log 10
                                    , b_cigar = toCigar ecig
                                    , b_mrnm  = invalidRefseq
                                    , b_mpos  = invalidPos
                                    , b_isize = 0
                                    , b_exts  = insertE "AS" (Float $ realToFrac scr) $
                                                insertE "NH"                      nh  $
                                                insertE "MD" (Text $          the_md) $
                                                insertE "SS" (Text $ sc_name scaffold) $
                                                b_exts frec } ]
  where
    hits = align conf $ bam_to_pps conf_database dmodel frec
    frec = if isReversed qrec then flipRecord qrec else qrec

    no_hits = foldr (\(Node l r, _, _) k -> l == r && k) True hits

    total_score = foldl1' plus [ scr - log (fromIntegral (r-l)) | (Node l r, scr, _) <- hits ]

    plus x y = if x <= y then x - log1p (exp (x-y)) else y - log1p (exp (y-x))

    nh = count_to conf_cnt_limit 0 hits
    count_to n acc             _ | acc > n = Text "lots"
    count_to _ acc [                     ] = Int acc
    count_to n acc ((Node l r, _, _) : hs) = count_to n (acc + (r-l)) hs

    dmodel = case (conf_lparam, conf_rparam) of
        (Nothing, Nothing) -> \_ _ -> (conf_div, 0, 0)
        (Nothing,  Just k) -> ss_model k k
        (Just l,  Nothing) -> ds_model l
        (Just l,   Just k) -> ss_model l k

    ss_model l k  i j = let ll = l ^ (i+1)
                            lr = k ^ (j+1)
                            lam = ll + lr - ll*lr
                        in (conf_div, conf_ss_deam * lam + conf_ds_deam * (1-lam), 0)

    ds_model l    i j = let ll = l ^ (i+1)
                            lr = l ^ (j+1)
                        in (conf_div, conf_ss_deam * ll + conf_ds_deam * (1-ll)
                                    , conf_ss_deam * lr + conf_ds_deam * (1-lr))


-- | Deamination/Divergence model
-- Input is distance from 5' end, distance from 3' end, output is
-- divergence, C->T probability, G->A probability
type DModel = Int -> Int -> (Double,Double,Double)

cleanInput :: BamRaw -> BamRec
cleanInput qraw = case unpackBam qraw of
    qr -> qr { b_exts = deleteE "AS" $ deleteE "MD" $ deleteE "NH" $ b_exts qr }

-- | Flips a 'BamRec' around the same way it's supposed to be done to
-- encode a reverse-strand alignment.  This is not general purpose!
-- 'b_pos', \"MD\" and probably others are mistreated.
flipRecord :: BamRec -> BamRec
flipRecord qrec = qrec { b_qual  = V.reverse <$> b_qual qrec
                       , b_seq   = V.reverse . V.map compls $ b_seq qrec
                       , b_flag  = flagReversed `xor` b_flag qrec
                       , b_cigar = V.reverse $ b_cigar qrec }

-- Brillant!
aln_length :: Op -> Int
aln_length (M _) = 1
aln_length  I    = 0
aln_length (D _) = 1

-- This stinks.
toMd :: Bool -> String -> [Op] -> B.ByteString

-- We aligned the forward sequence, so in @toMd True forward-sequence
-- ops@, @(reverse ops)@ describes this alignment.  So if the result is
-- on the forward strand, conversion is straight forward.
toMd True str = showMd . go str . reverse
  where
    go    s  (D c:ops) =              MdDel [w2n c] : go s ops
    go (d:s) (M c:ops) | d == w2c c = MdNum      1  : go s ops
                       | otherwise  = MdRep (w2n c) : go s ops
    go (_:s) (I  :ops)              =                 go s ops
    go   _       _                  = [                      ]

#if MIN_VERSION_biohazard(2,1,0)
    w2n = toNucleotides
#else
    w2n = toNucleotides . w2c
#endif

-- We aligned the forward sequence, but in @toMd False forward-sequence
-- ops@, @(reverse ops) describes the reversed alignment.   So we don't
-- need to reverse @ops@, but we need to reverse the sequence.  Where we
-- generate letters, they need to be complemented, but when
-- complementing, they don't.
toMd False str = showMd . go (reverse str)
  where
    go    s  (D c:ops)              = MdDel [w2nc c] : go s ops
    go (d:s) (M c:ops) | d == w2c c = MdNum       1  : go s ops
                       | otherwise  = MdRep (w2nc c) : go s ops
    go (_:s) (I  :ops)              =                  go s ops
    go   _       _                  = [                       ]

#if MIN_VERSION_biohazard(2,1,0)
    w2nc = toNucleotides . defaultComplFunc
#else
    w2nc = toNucleotides . w2c . defaultComplFunc
#endif


toCigar :: [Op] -> W.Vector Cigar
toCigar = V.fromList . map (\xs -> head xs :* length xs) . group . map go
  where
    go (D _) = Del
    go (M _) = Mat
    go  I    = Ins

