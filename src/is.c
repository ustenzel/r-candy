/*
 * sais.c for sais-lite
 * Copyright (c) 2008 Yuta Mori All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <sys/mman.h>
#include <unistd.h>

/* We will need a lot of arrays that can range further than 4G, but are
 * not needlessly large.  So here's a pointer to 5-byte signed integers
 * in little endian format. */

typedef struct _int5 { unsigned char i[4] ; signed char j ; } int5 ;

static inline long get5( const int5* p ) 
{
    return (long)(p->i[0])
        | ((long)(p->i[1]) << 8)
        | ((long)(p->i[2]) << 16)
        | ((long)(p->i[3]) << 24)
        | ((long)(p->j)    << 32) ;
}

static inline void put5( int5* p, long a ) 
{
    p->i[0] = (a >>  0) & 0xff ;
    p->i[1] = (a >>  8) & 0xff ;
    p->i[2] = (a >> 16) & 0xff ;
    p->i[3] = (a >> 24) & 0xff ;
    p->j    = (a >> 32) & 0xff ;
}

static inline void inc5( int5* p )
{
    put5( p, 1+get5(p) ) ;
}

static inline long dec5( int5* p )
{
    long a = get5(p)-1 ;
    put5(p,a) ;
    return a ;
}

typedef unsigned char ubyte_t;
#define chr(i) (cs == 5 ? get5(((const int5*)T)+i) : ((const unsigned char *)T)[i])

/* file-backed malloc */
/* Note conspicuous absence of error checking... */
/*
void *mapalloc( size_t sz )
{
    char *nm ;
    void *p = 0 ;
    if( 0 < asprintf( &nm, "%s/arrayXXXXXX", getenv( "TEMP" ) ) ) {
        printf( "%s (%ld)\n", nm, sz ) ;
        int fd = mkstemp( nm ) ;
        if( fd >= 0 ) {
            if( ftruncate( fd, sz ) == 0 ) {
                p = mmap( 0, sz, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0 ) ;
            }
            unlink( nm ) ; 
            close( fd ) ;
        }
    }
    return p ;
}

void mapfree( void* pe, void* p )
{
    munmap( p, pe-p ) ;
}
*/

/* find the start or end of each bucket */
static void getCounts(const void *T, int5 *C, long n, long k, int cs)
{
	long i;
	for (i = 0; i < k; ++i) put5(C+i,0);
	for (i = 0; i < n; ++i) inc5(C+chr(i));
}
static void getBuckets(const int5 *C, int5 *B, long k, int end)
{
	long i, sum = 0;
	if (end) {
		for (i = 0; i < k; ++i) {
			sum += get5(C+i);
			put5(B+i, sum);
		}
	} else {
		for (i = 0; i < k; ++i) {
			sum += get5(C+i);
			put5(B+i, sum - get5(C+i));
		}
	}
}

/* compute SA */
static void induceSA(const void *T, int5 *SA, int5 *C, int5 *B, long n, long k, int cs)
{
	int5 *b;
	long  i, j, c0, c1;
	/* compute SAl */
	if (C == B) getCounts(T, C, n, k, cs);
	getBuckets(C, B, k, 0);	/* find starts of buckets */
	j = n - 1;
	b = SA + get5(B + (c1 = chr(j)));
	put5(b++, ((0 < j) && (chr(j - 1) < c1)) ? ~j : j);
	for (i = 0; i < n; ++i) {
		j = get5(SA+i), put5(SA+i, ~j);
		if (0 < j) {
			--j;
			if ((c0 = chr(j)) != c1) {
				put5(B+c1, b - SA);
				b = SA + get5(B+ (c1 = c0));
			}
			put5(b++, ((0 < j) && (chr(j - 1) < c1)) ? ~j : j);
		}
	}
	/* compute SAs */
	if (C == B) getCounts(T, C, n, k, cs);
	getBuckets(C, B, k, 1);	/* find ends of buckets */
	for (i = n - 1, b = SA + get5(B + (c1 = 0)); 0 <= i; --i) {
		if (0 < (j = get5(SA+i))) {
			--j;
			if ((c0 = chr(j)) != c1) {
				put5(B+c1, b - SA);
				b = SA + get5(B+(c1 = c0));
			}
			put5(--b, ((j == 0) || (chr(j - 1) > c1)) ? ~j : j);
		} else put5(SA+i, ~j);
	}
}

/*
 * find the suffix array SA of T[0..n-1] in {0..k-1}^n use a working
 * space (excluding T and SA) of at most 2n+O(1) for a constant alphabet
 */
int sais_main(void (*progress)(long), const void *T, int5 *SA, long fs, long n, long k, int cs)
{
    assert( sizeof( int5 ) == 5 ) ;

	int5 *C, *B, *RA;
	long  i, j, c, m, p, q, plen, qlen, name;
	long  c0, c1;
	long  diff;

    progress( n ) ;

	/* stage 1: reduce the problem by at least 1/2 sort all the
	 * S-substrings */
	if (k <= fs) {
		C = SA + n;
		B = (k <= (fs - k)) ? C + k : C;
	} else if ((C = B = (int5 *) malloc(k * sizeof(int5))) == NULL) return -2;
	getCounts(T, C, n, k, cs);
	getBuckets(C, B, k, 1);	/* find ends of buckets */
	for (i = 0; i < n; ++i) put5(SA+i, 0);
	for (i = n - 2, c = 0, c1 = chr(n - 1); 0 <= i; --i, c1 = c0) {
		if ((c0 = chr(i)) < (c1 + c)) c = 1;
		else if (c != 0) put5(SA+(dec5(B+c1)), i + 1), c = 0;
	}
	induceSA(T, SA, C, B, n, k, cs);
	if (fs < k) free(C) ; // munmap( C, k*sizeof(long) ) ; // free(C);
	/* compact all the sorted substrings into the first m items of SA
	 * 2*m must be not larger than n (proveable) */
	for (i = 0, m = 0; i < n; ++i) {
		p = get5(SA+i);
		if ((0 < p) && (chr(p - 1) > (c0 = chr(p)))) {
			for (j = p + 1; (j < n) && (c0 == (c1 = chr(j))); ++j);
			if ((j < n) && (c0 < c1)) put5(SA+(m++), p);
		}
	}
	for (i = m; i < n; ++i) put5(SA+i, 0);	/* init the name array buffer */
	/* store the length of all substrings */
	for (i = n - 2, j = n, c = 0, c1 = chr(n - 1); 0 <= i; --i, c1 = c0) {
		if ((c0 = chr(i)) < (c1 + c)) c = 1;
		else if (c != 0) {
			put5(SA + (m + ((i + 1) >> 1)), j - i - 1);
			j = i + 1;
			c = 0;
		}
	}
	/* find the lexicographic names of all substrings */
	for (i = 0, name = 0, q = n, qlen = 0; i < m; ++i) {
		p = get5(SA+i), plen = get5(SA + (m + (p >> 1))), diff = 1;
		if (plen == qlen) {
			for (j = 0; (j < plen) && (chr(p + j) == chr(q + j)); j++);
			if (j == plen) diff = 0;
		}
		if (diff != 0) ++name, q = p, qlen = plen;
		put5(SA+(m + (p >> 1)), name);
	}

	/* stage 2: solve the reduced problem recurse if names are not yet
	 * unique */
	if (name < m) {
		RA = SA + n + fs - m;
		for (i = n - 1, j = m - 1; m <= i; --i) {
			if (get5(SA+i) != 0) put5(RA+(j--), get5(SA+i) - 1);
		}
		if (sais_main(progress, RA, SA, fs + n - m * 2, m, name, sizeof(int5)) != 0) return -2;
		for (i = n - 2, j = m - 1, c = 0, c1 = chr(n - 1); 0 <= i; --i, c1 = c0) {
			if ((c0 = chr(i)) < (c1 + c)) c = 1;
			else if (c != 0) put5(RA+(j--), i + 1), c = 0; /* get p1 */
		}
		for (i = 0; i < m; ++i) put5(SA+i, get5(RA+get5(SA+i))); /* get index */
	}
	/* stage 3: induce the result for the original problem */
    progress( -n ) ;
	if (k <= fs) {
		C = SA + n;
		B = (k <= (fs - k)) ? C + k : C;
	} else if ((C = B = (int5 *) malloc(k * sizeof(int5))) == NULL) return -2;
	/* put all left-most S characters into their buckets */
	getCounts(T, C, n, k, cs);
	getBuckets(C, B, k, 1);	/* find ends of buckets */
	for (i = m; i < n; ++i) put5(SA+i, 0); /* init SA[m..n-1] */
	for (i = m - 1; 0 <= i; --i) {
		j = get5(SA+i), put5(SA+i, 0);
		put5(SA+dec5(B+chr(j)), j);
	}
	induceSA(T, SA, C, B, n, k, cs);
	if (fs < k) free(C) ; // munmap( C, k*sizeof(long) ); // free(C);
	return 0;
}

/**
 * Constructs the suffix array of a given string.
 * @param T[0..n-1] The input string.
 * @param SA[0..n] The output array of suffixes.
 * @param n The length of the given string.
 * @return 0 if no error occurred
 */

// int is_sa(const ubyte_t *T, long *SA, long n)
// {
	// if ((T == NULL) || (SA == NULL) || (n < 0)) return -1;
	/* SA[0] = n;
	if (n <= 1) {
		if (n == 1) SA[1] = 0;
		return 0;
	} */
	// return sais_main(T, SA, 0, n, 256, 1);
// }

/**
 * Constructs the burrows-wheeler transformed string of a given string.
 * @param T[0..n-1] The input string.
 * @param n The length of the given string.
 * @return The primary index if no error occurred, -1 or -2 otherwise.
 */
/*
int is_bwt(ubyte_t *T, long n)
{
	long *SA, i, primary = 0;
	SA = (long*)calloc(n+1, sizeof(long));
	is_sa(T, SA, n);

	for (i = 0; i <= n; ++i) {
		if (SA[i] == 0) primary = i;
		else SA[i] = T[SA[i] - 1];
	}
	for (i = 0; i < primary; ++i) T[i] = SA[i];
	for (; i < n; ++i) T[i] = SA[i + 1];
	free(SA);
	return primary;
}
*/
